import {ADD_PENDING, DEL_PENDING, SHOW_INFO, DELETE_ITEM} from './actionType';
import {getDataFromServerToClient} from './fect_data_surat';
import {store_info} from './reducer';

export {
    store_info,
    SHOW_INFO,
    ADD_PENDING, 
    DEL_PENDING,
    getDataFromServerToClient,
    DELETE_ITEM   
}