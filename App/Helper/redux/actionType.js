/* 
    action type for redux operation 

*/


export const GET_DATA_BEGIN = 'GET_DATA_PROCESS';
export const ADD_PENDING = 'ADD_PENDING';
export const DEL_PENDING = 'DEL_PENDING';
export const GET_DATA_SUCCESS = 'SHOW_INFO';
export const GET_DATA_FAILED = 'GET_DATA_FAILED';
export const DELETE_ITEM = 'DEL_ITEM_STATE';