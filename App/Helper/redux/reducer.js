import {ADD_PENDING, DEL_PENDING, GET_DATA_BEGIN, GET_DATA_SUCCESS, GET_DATA_FAILED, DELETE_ITEM} from './actionType';
import { get_data_failure } from './actions';


/*  REDUX STATE MANAGEMENT HERE */
let STATE_VALUE = {
    loading : false,
    error : '',
    surat_pending : 0,
    surat_rejected: 0,
    surat_terbuat : 0,
    grettiings    : '',
    nama          : '',
    pemberitahuan : [],
};


/*  create reducer store  */
export const store_info = (state = STATE_VALUE, actions) => {    
    let temp = {}, minValue = 0;
    switch (actions.type) {
        case  GET_DATA_BEGIN : 
            temp = {
                ...state,
                loading : true, 
            };
            break;
        case GET_DATA_SUCCESS :
            temp = {
                ...state,
                loading : false, 
                surat_pending : actions.payload.surat_pending,
                surat_rejected: actions.payload.surat_rejected,
                surat_terbuat : actions.payload.surat_terbuat,
                grettiings    : actions.payload.grettiings,
                nama          : actions.payload.nama,
                pemberitahuan : actions.payload.pemberitahuan
            }   

            break;
        case GET_DATA_FAILED :  
            temp = {
                ...state,
                loading : false, 
                error : actions.error
            }
            break;
        case ADD_PENDING :  
            temp = {
                ...state, 
                surat_pending : state.surat_pending + 1
            };
            break;

        case DEL_PENDING : 
            if(state.surat_pending > 0) {
                temp = {
                    ...state, 
                    surat_pending: state.surat_pending - 1,
                };
            } else {
                temp = {
                    ...state,
                    surat_pending : minValue,
                };
            }
            break;
        case DELETE_ITEM : 
            state= STATE_VALUE;
            break;
        default:
            temp = state;
    }
    return temp;
}


