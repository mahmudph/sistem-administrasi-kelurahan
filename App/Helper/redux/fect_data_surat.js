import {surat_analysis, get_pemberitahuan_info} from '@Apis';
import { get_data_begin, get_data_success,  get_data_failure } from './actions'

/* get data from server then save to redux */
export function getDataFromServerToClient () {

    return async (dispatch) => {
        dispatch(get_data_begin());
        console.log('hohohoho');
    
        try {
            let surat_info          = await surat_analysis();
            let pemberitahuan_info  = await get_pemberitahuan_info();
            let data = {
                surat_pending : surat_info.data.data.surat_pending,
                surat_rejected: surat_info.data.data.surat_rejected,
                surat_terbuat : surat_info.data.data.surat_selesai,
                grettiings    : surat_info.data.salam[0] + surat_info.data.salam.slice(1).toLowerCase(),
                nama          : surat_info.data.nama.toLowerCase(),
                pemberitahuan : pemberitahuan_info.data.data.map((item ) => {
                  let data    = item; 
                  item.title  = item.title[0].toUpperCase() + item.title.slice(1)
                  return data
                })
            }

            console.log('im heree! cheerrr');
            dispatch(get_data_success(data));
            return data;

        } catch (error) {
            let err  = 'tidak ada koneksi'; 
            dispatch(get_data_failure(err));
        }
    }
}

