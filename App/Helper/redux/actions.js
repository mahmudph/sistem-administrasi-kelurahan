import {GET_DATA_FAILED, GET_DATA_BEGIN, GET_DATA_SUCCESS} from './actionType';


function get_data_begin() {
    return {
        type : GET_DATA_BEGIN
    }
}

function get_data_success(data) {
    return {
        type : GET_DATA_SUCCESS,
        payload : data
    }
}

function get_data_failure(err) {
    return {
        type : GET_DATA_FAILED,
        error : err
    }
}


export {
    get_data_begin,
    get_data_success, 
    get_data_failure,
}