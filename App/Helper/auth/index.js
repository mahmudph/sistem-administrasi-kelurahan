import {AsyncStorage} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import ImagePicker from 'react-native-image-picker'
import {uploadtoServer} from '../uploadImage';
import config from '../../Configs/config';
import { createStore} from 'redux'


/* 
@ this modele used to make validation when user is login or logout 
@ check sessinon in memori if exsist continue to dashboard 
@ else continue to the login screen 

*/

export const checkToken = async () => {
    const token = await AsyncStorage.getItem('token');
    console.log(token, 'ini tokennya mas');
    return token != '' ? token : null;
}


export const resetNavigation = async (rotuerName) => {
    try {
        const navigateAction = await StackActions.reset({
            index:0,   
            actions: [NavigationActions.navigate({ 'routeName': rotuerName })],
        });
        return navigateAction;
    } catch (err) {
        throw err;
    }
}


export const choseActionNew = async (id) => {
    let aksi = null;
    switch(id) {
        case 1 : 
            aksi = 'General';
            break;
        case 12 : 
            aksi = 'TidakMampu';
            break;
        case 13 : 
            aksi = 'Pernyataan';
            break;
        case 38 : 
            aksi = "RujukCerai";
            break;
        case 33 : 
            aksi = "Married";
            break;
        case 13 :
            aksi = "Pernyataan";
            break;
        case 26 : 
            aksi = 'Married';
            break;
        case 24 : 
            aksi = 'Death';
            break;
    }

    return aksi;
}


export const choseActionUpdate = function (id) {
    let aksi  = null;
    switch(id) {
        case '1' : 
            aksi = 'GeneralUpdate';
            break;
        case '12' : 
            aksi = 'Kurang_mampu';
            break;
        case '13' : 
            aksi = 'Pernyataan_update';
            break;
        case '38' : 
            aksi = "RujukCeraiUpdate";
            break;
        case '33' : 
            aksi = "Married_update";
            break;
        case '37' :
            aksi = "Devorce_update";
            break;  
        case '26' : 
            aksi = 'Married_update';
            break;
        case '24' : 
            aksi = 'Kematian_update';
            break;
        default :
            aksi = "sfsdf";
            break;
    }

    return aksi;
}


export const takePictureandSend = async(type, cb) => {
    var result = "";
    try{
        const options = {
            quality: 1.0,
            storageOptions: {
              skipBackup: true
            }
        };
        ImagePicker.launchCamera(options, async (res) => {
            let form   = await new FormData();
            if (type == 'kk') {
                form.append('foto_kk', { 
                    'uri' : res.uri,
                    'name' : res.fileName,
                    'type' : res.type
                });
                let data = await uploadtoServer(form);
                result = config.url_assets + '/assets/files/dokumen/' + data.image_kk;
                return cb(null, result);
            } else {
                form.append('foto_ktp', { 
                    'uri' : res.uri,
                    'name' :res.fileName,
                    'type' :res.type
                });
                let data = await uploadtoServer(form);
                result = config.url_assets + '/assets/files/dokumen/' + data.image_pbb;
                return cb(null, result);
            }

        });

    }catch(er) {
        return cb('error ketika mengambil foto', null);
    }

}


export const getPictureFromLibrary = async (type, cb) => {
    try {
        const options = {
            quality: 1.0,
            storageOptions: {
              skipBackup: true
            }
        };

        ImagePicker.launchImageLibrary(options, async (res) => {
            let form   =  await new FormData();
            if (type == 'kk') {
                form.append('foto_kk', { 
                    'uri' : res.uri,
                    'name' : res.fileName,
                    'type' : res.type
                });
                let data = await uploadtoServer(form);
                result   = config.url_assets + '/assets/files/dokumen/' + data.image_kk;
                return cb(null, result);
             
            } else {
                form.append('foto_ktp', { 
                    'uri' : res.uri,
                    'name' :res.fileName,
                    'type' :res.type
                });
                let data = await uploadtoServer(form);
                result   = config.url_assets + '/assets/files/dokumen/' + data.image_pbb;
                return cb(null, result);
            }
        });
    } catch (err) {
        return cb('error ketika mengambil foto dari galeri');
    }
}

