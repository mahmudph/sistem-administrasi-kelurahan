/* 
    file for upload image
*/

import {launchCamera} from 'react-native-image-picker'
import {URI_UPLOAD_IMAGE } from '../Configs/Api';
import {checkToken} from '@AuthCheck';


const uploadtoServer = (data) => {
  return checkToken().then(token => {
    var options = {
        method : 'POST',
        body : data,
        headers : {
            'Content-Type': 'multipart/form-data',
            'Accept': 'application/json',
            'authorization' : token,
        }
    }
    return fetch(URI_UPLOAD_IMAGE, options).then(res => res.json()).then(data => {
      return data;
    }).catch(err => {
      console.log(err);
    })
  });
    
};


export {
    uploadtoServer,
}