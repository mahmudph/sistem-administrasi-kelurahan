/* 
    file setting environment aplikasi
    file ini berfungsi sebagai environmeent pengembangan react native
    terdapat dua jenis yakni development dan production
*/

const pengembangan = 'production';


const modules = {
    development :  {
        url_api : 'http://192.168.43.213/kelurahan1/index.php',
        url_assets : 'http://192.168.43.213/kelurahan1',
        assert : '../assets/'
    },

    production : {
        url_api : 'https://16ulu.com',
        url_assets : 'https://16ulu.com',
        assert : '../assets/'
    }
}

if(pengembangan == null) throw new Error('select type pengembangan program');

export default modules[pengembangan];