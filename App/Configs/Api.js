import convigEnv from './config';

/* 
    konfigurasi seluruh api yang ada dlam pengembangan react-native 
    meliputi post data maupun get data 

*/
const BASE_URL = convigEnv['url_api'];


/* NEW LINK API  */
export const URI_WARGA_ANALYSIS         = BASE_URL + '/api/warga/get_warga_count';
export const URI_WARGA_SEARCH           = BASE_URL + '/api/warga/get_warga_limit_15';
export const URI_WARGA_MENINGGAL        = BASE_URL + '/api/warga/get_all_warga_meninggal';
export const URI_WARGA_DESCRIPTION      = BASE_URL + '/api/warga/get_info_warga?id=';
export const URI_WARGA_NEW_DEATH        = BASE_URL + '/api/warga/new_warga_mati';
export const URI_SURAT_ANALYSIS         = BASE_URL + '/api/surat/get_analysis_surat';
export const URI_SURAT_NEW_PENGANTAR    = BASE_URL + '/api/surat/new_pengantar';
export const URI_LOG_SURAT_REJECTED_LIST= BASE_URL + '/api/logs/log_surat_rejected';
export const URI_LOGS_SURAT_TERBUAT     = BASE_URL + '/api/logs/log_surat_terbuat';
export const URI_LOGS_SURAT_FINISHED    = BASE_URL + '/api/logs/log_surat_selesai';
export const URI_UPLOAD_IMAGE           = BASE_URL + '/api/home/upload_image';
export const URL_CHANGE_NOMER           = BASE_URL + '/api/home/changeNomerRt';
export const URL_REGISTER               = BASE_URL + '/api/auth/registrasiAccount';
export const URL_LOGIN                  = BASE_URL + '/api/auth/loginProcess';
export const URL_CHANGE_PASSWORD        = BASE_URL + '/api/auth/changeFirstPassword';
export const URL_TOKEN_AUTH             = BASE_URL + '/api/auth/AuthToken';
export const URL_PROFILE_USER           = BASE_URL + '/api/home/get_info_rt';
export const URL_FOR_AUTH_APP           = BASE_URL + '/api/auth/checkToken_open';
export const URL_FOR_UPDATE_SURAT       = BASE_URL + '/api/surat/get_surat_by_id';
export const URL_FOR_DELETE_SURAT       = BASE_URL + '/api/surat/hapus_surat';
export const URL_FOR_UPDATE_SAVE_SURAT  = BASE_URL + '/api/surat/surat_pengantar_update';
export const URL_PEMBERITAHUAN_INFO     = BASE_URL + '/api/home/pemberitahuan_info';