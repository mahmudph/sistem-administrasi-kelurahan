import { 
  createAppContainer, 
  createStackNavigator,
  createSwitchNavigator 
} from 'react-navigation';
import SplashScreen from '../Containers/SplashScreen'
import Login from '../Containers/Auth/Login';
import SignUp from '../Containers/Auth/SignUp';
import ForgotPasswordAuth from '../Containers/Auth/ChangePasswordAuth';
import SearchNik from '../Containers/Homepage/Home/Content/SearchNik';
import General from '../Containers/Homepage/Home/Content/surat/General';
import Death from '../Containers/Homepage/Home/Content/surat/Death';
import TidakMampu from '../Containers/Homepage/Home/Content/surat/TidakMampu';
import Pernyataan from '../Containers/Homepage/Home/Content/surat/Pernyataan';
import Info_app from '../Containers/Homepage/More/info_app';
import Layout from '../Containers/Homepage/Layout';
import RujukCerai from '../Containers/Homepage/Home/Content/surat/RujukCerai';
import IntroApp from '../Containers/LandingPageIntro';


/* 
update form 

*/
import RujukCeraiUpdate from '../Containers/Homepage/Logs/Update_surat/Rujuk_cerai';
import GeneralUpdate from '../Containers/Homepage/Logs/Update_surat/Pengantar_update';
import Kematian_update from '../Containers/Homepage/Logs/Update_surat/Kematian_update';
import kurang_mampu_Update from '../Containers/Homepage/Logs/Update_surat/Kurang_mampu';
import Pernyataan_update from '../Containers/Homepage/Logs/Update_surat/Pernyataan';







/*  routes for autentication here */
const appAuth = createStackNavigator({
  IntroApp : {screen : IntroApp},
  Login    : {screen: Login},
  SignUp   : {screen : SignUp},
  ForgotPasswordAuth : {screen : ForgotPasswordAuth},
},{ defaultNavigationOptions: { header:null }});

/* this is for welcome when theres is no  */
const Appscreens = createStackNavigator({
  Home : {screen :Layout},
  General : {screen : General},
  Death : {screen : Death},
  RujukCerai : {screen: RujukCerai},
  RujukCeraiUpdate : {screen : RujukCeraiUpdate},
  GeneralUpdate : {screen : GeneralUpdate},
  Kematian_update : {screen : Kematian_update},
  Kurang_mampu : {screen : kurang_mampu_Update},
  Pernyataan_update : {screen : Pernyataan_update},
  Pernyataan : {screen: Pernyataan},
  TidakMampu : {screen: TidakMampu},
  Login    : {screen: Login},
  SearchNik : {screen : SearchNik},
  Info_app : {screen : Info_app},
  
},{ 
  defaultNavigationOptions: { header:null, headerMode: 'screen' },
  headerMode: 'screen' 

});

const RootStack = createSwitchNavigator({
    SplashScreen  : {screen : SplashScreen},
    Auth          : { screen: appAuth, navigationOptions: { header: null }},
    App           : { screen: Appscreens, navigationOptions: { header: null }}
},{ initialRouteName: 'SplashScreen' })

export default createAppContainer(RootStack);