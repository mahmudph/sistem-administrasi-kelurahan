
const allLogo = {
    splashscreen: require('./images/splashscreen.png'),
    icon : require('./images/logoApp.png'),
    newSurat : require('../assets/icons/paper.png'),
    human : require('../assets/icons/peoples.png'),
    notFound : require('../assets/icons/no_found_data.png'),
    mati : require('../assets/icons/death.png'),
    addWarga : require('../assets/icons/data_penduduk.png'),
    paper : require('../assets/icons/paper.png'),
    divorce : require('../assets/icons/cerai.png'),
    tidakmampu : require('../assets/icons/kurang_mampu.png'),
    success : require('../assets/lottie/success.json'),
    cerai_rujuk : require('../assets/icons/rujuk_cerai.png'),
    login : require('../assets/icons/auth/login.png'),
    register : require('../assets/icons/auth/register.png'),
    password : require('../assets/icons/auth/password.png'),
    kuser : require('../assets/icons/kuser.png')
  }

export {allLogo}