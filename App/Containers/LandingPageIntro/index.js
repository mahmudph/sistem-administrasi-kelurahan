import React, {Component} from 'react';
import { StyleSheet, View, Text, Image, Dimensions,StatusBar } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {mocks} from '../../constants/index';


const {width, height} = Dimensions.get('screen');
export default class App extends Component {
    constructor(props) {
      super(props);
      this.state = {
        showRealApp: false,
      };
    }
    componentWillMount() {
      StatusBar.setHidden(true);
    }

    _onDone = () => {
        this.props.navigation.navigate('Login');
    };
    _onSkip = () => {
        this.props.navigation.navigate('Login');
    };
    _renderItem = ({ item }) => {
      return (
        <View
          style={{
            width:width,
            height:height,
            flex: 1,
            backgroundColor: item.backgroundColor,
            alignItems: 'center',
            justifyContent: 'space-around',
            paddingBottom: 100
          }}>
          <Text style={styles.title}>{item.title}</Text>
          <Image style={styles.image} source={item.image} />
          <Text style={styles.text}>{item.text}</Text>
        </View>
      );
    };
    render() {
        return (
          <AppIntroSlider
            slides={mocks.slides}
            renderItem={this._renderItem}
            onDone={this._onDone}
            showSkipButton={true}
            onSkip={this._onSkip}
          />
        );
    }
  }
const styles = StyleSheet.create({
    image: {
        width: 200,
        height: 200,
    },
    text: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        paddingVertical: 30,
    },
    title: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginBottom: 16,
    },
});
   
