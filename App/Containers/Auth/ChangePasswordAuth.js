import React, {Component} from 'react';
import {withNavigation} from 'react-navigation';
import {
    Animated,
    Image,
    Alert, 
    View,
    ActivityIndicator, 
    Keyboard, 
    Dimensions,
    Text,
    StatusBar
} from 'react-native';
import { ChangePasswordAuth } from '@Apis';
import { Container,Toast,Item, 
  Form, Content, Icon, Input, Header, Left, Row, Col, Right, Title, Body,Button, Grid } from 'native-base';
import {allLogo} from '@assets';



const {width, height} = Dimensions.get('window');
class ForgotPasswordAuth extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            password : '',
            loading: false,
        }
    }


    componentWillMount () {
        StatusBar.setBarStyle( 'light-content',true);
        StatusBar.setBackgroundColor("#7DF86D");
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    }

 
    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        Animated.timing(this.imageHeight, {
        duration: event.duration,
        toValue: IMAGE_HEIGHT_SMALL,
        }).start();
    };

    keyboardWillHide = (event) => {
        Animated.timing(this.imageHeight, {
        duration: event.duration,
        toValue: IMAGE_HEIGHT,
        }).start();
    };


  handleChangePassword(){
      var body  = {
          id : this.props.navigation.getParam('itemUserId'),
          newpassword : this.state.password
      }
      if(body.newpassword.length > 6 ) {
        ChangePasswordAuth(body).then(result => {
            if(result.data.code == 1) {
                Alert.alert(
                    'Success!',
                    result.data.msg,
                    [
                      {
                        text: 'lanjutkan', onPress: () => {
                          this.props.navigation.replace('Login');
                        }
                      }
                    ],
                    { cancelable: false }
                  )
            } else {
                this.setState({loading:false});
                Alert.alert(
                    'gagal!',
                    result.data.msg,
                    [
                      {
                        text: 'Continue', onPress: () => {
                          console.log(result.data.msg)
                        }
                      }
                    ],
                    { cancelable: false }
                  )
            }
        });
      } else {
        Toast.show({
          text : 'password harus lebih dari 6 kata',
          buttonText : 'oke',
        })
      }


    }

    render() {
        return(
          <Container>
            <Content enableOnAndroid style={{height:height}}>
                <Grid style={{ alignItems:'center', justifyContent:'center'}}>
                  <Row style={{marginTop:20}}>
                    <Col>
                      <View style={{justifyContent:'flex-end', paddingHorizontal:20, alignItems:'center'}}>
                        <Text style={{fontSize:20}}>BUAT PASSWORD BARU</Text>
                        <Text style={{justifyContent:'center', alignItems:'center', textAlign:'center'}}>Pembuatan password baru dibutuhakan untuk mengamankan akun anda!</Text>
                      </View>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                        <View style={{justifyContent:'flex-end', paddingHorizontal:20, alignItems:'center'}}>
                          <Image source={allLogo.login} style={{width: 400, height:250}} resizeMode={'stretch'}/>
                      </View>
                    </Col>
                  </Row>
                  <Row>
                    <Col style={{marginHorizontal:10}}>
                        <Form>
                          <Item>
                            <Input
                              secure
                              label="Password"
                              placeholder="kata sandi"
                              underline
                              onChangeText={text => this.setState({ password: text })}
                            />
                            <Icon name={"lock"} style={{color:'#aaa'}}/>
                          </Item>
                        </Form>
                    </Col>
                  </Row>
                  <Row style={{ marginHorizontal:20}}> 
                    <Button block success onPress={() => this.handleChangePassword()} style={{alignItems:'center', justifyContent:'center', width:'100%', marginTop:20}}>
                      {this.state.loading ?
                        <ActivityIndicator size="small" color="white" /> : 
                        <Text style={{color:'#fff'}} bold white center>Ganti password</Text>
                      }
                    </Button>
                  </Row>
                </Grid>
            </Content>
          </Container>
          
        )
    }
}

export default ForgotPasswordAuth;