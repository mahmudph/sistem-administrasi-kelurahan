import React, { Component } from 'react';
import { 
  Alert, 
  ActivityIndicator, 
  Keyboard, 
  TouchableWithoutFeedback,
  StyleSheet,
  View,
  Image,
  Dimensions,
  StatusBar,
} from 'react-native';
import {
  Container,
  Button,
  Text,
  Form,
  Item as FormItem,
  Input,
  Content,
  Toast,
  Row,
  Col,
  Grid,
} from 'native-base';
import {postRegister} from '@Apis'
import {allLogo} from '@assets';



export default class SignUp extends Component {
  state = {
    username: null,
    token: null,
    loading: false,
  }
  passwordFocus = null;


  componentDidMount() {
    StatusBar.setBarStyle( 'light-content',true);
    StatusBar.setBackgroundColor("#fff");
  }


  handleSignUp = () => {
    const { navigation } = this.props;
    const { username, token } = this.state;
    Keyboard.dismiss();


    this.setState({ loading: true });
    if (username && token) {
      this.setState({ loading: true });
      let body = {
        'username' : username,
        'token' : token
      }
        postRegister(body)
        .then(res => {
          if(res.data.code == 1) {
            Alert.alert(
              'Berhasil!',
              'Ganti password anda untuk melanjutkan!',
              [
                {
                  text: 'lanjutkan', onPress: () => {
                    navigation.replace('ForgotPasswordAuth', {
                      itemUserId : res.data.user
                    });
                  }
                }
              ],
              { cancelable: false }
            )
            
          } else {
            Alert.alert(
              'Gagal!',
              'Username atau pin token tidak ditemukan',
              [
                {
                  text: 'oke', onPress: () => {
                    this.setState({loading:false});
                    
                  }
                }
              ],
              { cancelable: false });
              
          }
        }).catch(err => {
          Toast.show({
            text : 'pastikan anda memiliki koneksi internet',
            type : 'info',
            buttonText : 'oke',
          })  
        });
    } else {
      this.setState({loading:false});
      Toast.show({
        text : 'input field tidak lengkap',
        buttonText : 'oke',
      })
    }
  }

  render() {
    return (
      <Container style={{backgroundColor:'#fff'}}>
          <Content enableOnAndroid>
            <Grid>
              <Row>
                <Col>
                  <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:20}}>
                        <Text h2 style={{color:'#000', fontSize:28}}>Aktifasi Akun </Text>
                  </View>
                </Col>
              </Row>
              <Row>
                <Col>
                  <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                      <Image source={allLogo.register} style={{width: 400, height:275}} resizeMode={'stretch'}/>
                  </View>
                </Col>
              </Row>
              <Row style={{ marginHorizontal:20}}>
                <Col>
                <Form >
                    <FormItem regular >
                      <Input
                          returnKeyType='next'
                          autoCorrect={false}
                          autoCompleteType={false}
                          placeholder={'  Username'}
                          value={ this.state.username}
                          placeholderTextColor={'#aaa'}
                          onChangeText={(text => {
                            this.setState({ username : text })
                          })}
                          onSubmitEditing={()=> this.passwordFocus._root.focus()}
                        />
                    </FormItem>
                    <FormItem style={{marginTop:10}} regular>
                      <Input
                        ref={ref => (this.passwordFocus = ref)}
                        autoCorrect={false}
                        placeholderTextColor={'#aaa'}
                        autoCompleteType={false}
                        placeholder="  Pin"
                        value={this.state.token}
                        onChangeText={text => this.setState({ token: text })}
                      />
                    </FormItem>
                  </Form>
                </Col>
              </Row >
              <Row style={{flex:1,paddingHorizontal:20, marginTop:10}}>
                  <Button block   onPress={() => this.handleSignUp()} style={{marginBottom:10,  flex:1, flexDirection:'column', alignItems:'center'}}>
                    {this.state.loading ?
                      <ActivityIndicator size="small" color="white" /> : 
                      <Text bold white center>Aktifasi Akun </Text>
                    }
                </Button>
              </Row>
              <Row style={{flex:1,paddingHorizontal:20, }}>
                <Col>
                  <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Login')} style={{justifyContent:'flex-end', alignContent:'flex-end', alignItems:'flex-end'}}>
                    <Text caption center white style={{color:'#000'}} >
                     Sudah Terdaftar?
                      <Text style={{textDecorationStyle:'solid', color:'#aaa'}}> Login</Text> 
                    </Text>
                  </TouchableWithoutFeedback>

                </Col>
              </Row>
            </Grid>
          </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    // position: 'absolute',
    // left: 0,
    // top: 0,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  login: {
    flex: 1,
    justifyContent: 'center',
    textAlign : 'center',
    paddingTop:40,
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomWidth: StyleSheet.hairlineWidth,
    fontWeight :'normal',
    
  },
});
