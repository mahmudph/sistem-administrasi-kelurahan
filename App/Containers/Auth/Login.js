import React, { Component } from 'react'

import { 
  ActivityIndicator, 
  Keyboard, 
  StyleSheet, 
  Image,
  View,
  AsyncStorage,
  StatusBar,
  Dimensions,
  TouchableWithoutFeedback,
  KeyboardAvoidingView
} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { resetNavigation } from '@AuthCheck';
import {
  Container,
  Button,
  Text,
  Form,
  Item as FormItem,
  Input,
  Content,
  Toast,
  Grid,
  Row,
  Col,
} from 'native-base';
import {postLogin, GetProfile} from '@Apis'
import Icon from 'react-native-vector-icons/dist/Ionicons';
import OneSignal from 'react-native-onesignal';
import {allLogo} from '@assets';

const {width, height} = Dimensions.get('window');


export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null,
      loading: false,
      scuerePassword : true,
      errors : [],
      device_id : '',
      errorname : false,
      errorpassword : false,
    }
    this.passwordFocus = null;
    this.nama = null;
    
  }

  componentDidMount() {
    StatusBar.setBarStyle( 'light-content',true);
    StatusBar.setBackgroundColor("#fff");
    OneSignal.init("dd5cf332-2c70-4a3c-a20c-b9b8e4a7867a");
    OneSignal.addEventListener('ids', this.onIds.bind(this));
  }

  onIds(device) {
    this.setState({device_id : device.userId});
    
  }

  focus(text) {
    this.passwordFocus._root.focus()
  }
  handleLogin = async () => {
   try {

    if(this.state.username && this.state.password) {
      this.setState({ loading: true, errorname: false, errorpassword:false });
      Keyboard.dismiss();
      var data ={};
      data['username'] = this.state.username;
      data['password'] = this.state.password;
      data['device']   = this.state.device_id;

      const result      = await postLogin(data);
      if(result.data.code == 1) {
        await AsyncStorage.setItem('token', result.data.token);
        
        const getProfile  = await GetProfile();
        await AsyncStorage.setItem("Profile", JSON.stringify(getProfile.data.data));
        await AsyncStorage.setItem('nomer', getProfile.data.data.phone);
      
        resetNavigation('Login').then(res => {
          this.props.navigation.dispatch(res);
          this.props.navigation.navigate('Home');
        });
      } else {  
          this.setState({loading : false});
          Toast.show({
            text : 'Username atau Password tidak ditemukan',
            buttonText : 'oke',
          })
        }
    } else {
      this.setState({errorpassword : true, errorname:true});
      Toast.show({
        text : 'input tidak boleh kosong',
        buttonText : 'oke',
      });
      this.setState({loading: false})
    }    
   } catch (err) {
    this.setState({ loading: false });
    Toast.show({
      text : err,
      buttonText : 'oke',
    });
   }

  }

  render() {
    const { loading } = this.state;
    return (
      <Container style={[styles.backgroundImage,{backgroundColor:'#fff'}]}>
          <Content  enableOnAndroid >
            <Grid style={{flex:1, alignItems:'center',justifyContent:'center'}}>
              <Row style={{marginTop:20}}>
                <Col>
                  <View style={{justifyContent:'center', paddingHorizontal:20, alignItems:'center'}}>
                      <Text style={{fontSize:28, color:'#000'}}>Login</Text>
                  </View>
                </Col>
              </Row>
              <Row>
                <Col>
                  <View style={{justifyContent:'flex-end', paddingHorizontal:20, alignItems:'center'}}>
                      <Image source={allLogo.login} style={{width: 400, height:275}} resizeMode={'stretch'}/>
                  </View>
                </Col>
              </Row>
              <Row  style={{paddingHorizontal:20}}>
                <Col style={{justifyContent:'flex-start'}}>
                  <KeyboardAvoidingView behavior="padding">
                  <Form>
                      <FormItem error={this.state.errorname} regular >
                      {/* <Icon name="user" style={{color:'#ccc'}} style={{color:'#fff', fontSize:20}}/> */}
                        <Input
                            returnKeyType='next'
                            placeholder={'masukan akun anda'}
                            onChangeText={(text => {
                              this.setState({ username : text })
                            })}
                            placeholderTextColor={'#aaa'}
                            autoCorrect={false}
                            onSubmitEditing={(event) => { this.focus(); }}
                          />
                      </FormItem>
                      <FormItem error={this.state.errorpassword} regular style={{marginTop:10}}>
                          <Input
                            secure
                            ref={(text) => this.passwordFocus = text}
                            returnKeyType="done"
                            placeholder=" kata sandi"
                            placeholderTextColor={'#aaa'}
                            secureTextEntry={this.state.scuerePassword} 
                            // autoCapitalize={false}
                            autoCorrect={false}
                            onChangeText={text => this.setState({ password: text })}
                          />
                          <Button transparent onPress={() => this.setState({scuerePassword: !this.state.scuerePassword})}>
                            {this.state.scuerePassword ? 
                            <Icon name="md-eye" style={{color:'#aaa', fontSize:20, paddingRight:20}} />
                            : <Icon name="md-eye-off" style={{color:'#aaa', fontSize:20, paddingRight:20}} />
                            }
                          </Button>
                        </FormItem>
                    </Form>
                  </KeyboardAvoidingView>
                </Col>
              </Row>
              <Row style={{flex:1,paddingHorizontal:20, marginTop:5}}>
                <View style={{width:'100%'}}>
                  <Button block onPress={() => this.handleLogin()} style={{marginVertical:5, width:'100%'}}>
                      {loading ?
                        <ActivityIndicator size="small" color="white" /> : 
                        <Text bold white center>Masuk</Text>
                      } 
                  </Button>
                </View>
              </Row>
              <Row style={{flex:1,paddingHorizontal:20, }}>
                <Col>
                  <TouchableWithoutFeedback  onPress={() => this.props.navigation.navigate('SignUp')} style={{}}>
                        <Text caption center white style={{color:'#000', justifyContent:'flex-end', alignContent:'flex-end', alignItems:'flex-end'}} >
                          Belum Terdaftar ?
                          <Text style={{textDecorationStyle:'solid', color:'#aaa'}}> Aktifasi Akun</Text> 
                        </Text>
                  </TouchableWithoutFeedback>

                </Col>
              </Row>
            </Grid>
          </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: width,
    height: height,
  },
  login: {
    flex: 1,
    justifyContent: 'center',
    textAlign : 'center',
    paddingTop:40,
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    fontWeight :'normal',
    
  },
});
