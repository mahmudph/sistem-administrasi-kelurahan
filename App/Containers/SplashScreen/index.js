import React,{ Component } from 'react';
import { allLogo } from "@assets";
import {checkToken} from '@AuthCheck';
import {
    StyleSheet,
    ImageBackground, 
    Dimensions, 

} from 'react-native';
import { Container } from 'native-base';
const { width, height } = Dimensions.get('window');

export default class SplashScreen extends Component{
  constructor(props) {
    super(props);
    this.state = {
      alert : false
    }

  }

  componentWillMount = async () =>{
    const token      = await checkToken();
    setTimeout(() => {  
      if(token) {
            this.props.navigation.navigate('App');
      } else {
            this.props.navigation.navigate('Auth');
      }
    }, 3000);
  }

  render() {
    return (  
      <Container>
        <ImageBackground source={allLogo.splashscreen} style={styles.splashscreen} resizeMode={'cover'} />
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  splashscreen: {
    width,
    height,
  },
})
