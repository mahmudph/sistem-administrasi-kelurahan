import React,{Component} from 'react';
import {View,Image, StatusBar} from 'react-native';
import {
    Container,Header,Grid, Row,Col,Button,
    Body, Left, Right, Title, Content,Icon,Text
} from 'native-base';
import { withNavigation } from 'react-navigation';
import {allLogo} from '@assets';

class Info_app extends Component {
    constructor(props) {
        super(props);
        this.state = {
            app_version : 'BETA 1.0.0',
            build_by : 'BDPRO',
            title : "SISTEM PELAYANAN ENAM BELAS ULU"
        }
    }
    componentDidMount() {
        StatusBar.setBarStyle( 'light-content',true);
        StatusBar.setBackgroundColor("#64e291");
    }

    render() {
        const navigation = this.props.navigation;
        return(
            <Container>
                <Header style={{backgroundColor:'#64e291', zIndex:100000}}>
                    <Left>
                        <Button transparent onPress={() => navigation.goBack()}>
                            <Icon name="arrow-back" style={{size:20}}/> 
                        </Button>
                    </Left>
                    <Body>
                        <Title>Info Aplikasi</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <Grid>
                        <Row>
                            <Col>
                                <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:100}}>
                                    <Image source={allLogo.icon} style={{width:150, height:150, alignItems:'center', justifyContent:'center'}}/>
                                    <Text h1 style={{marginVertical:5}}>{this.state.title}</Text>
                                    <Text h2>APP VERSION {this.state.app_version}</Text>
                                    <Text h3>DIBUAT OLEH {this.state.build_by}</Text>
                                </View>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }

}

export default withNavigation(Info_app);