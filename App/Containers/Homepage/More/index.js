import React, { Component } from 'react'
import { 
  Image,
  StyleSheet, 
  ScrollView, 
  AsyncStorage,
  Linking,
  View,
  Alert,
  Dimensions,
  Modal,
  TouchableWithoutFeedback,
  TextInput,
  KeyboardAvoidingView,
  ToastAndroid,
  StatusBar
} from 'react-native'
import { allLogo } from '@assets' 
import { withNavigation } from 'react-navigation';
import { 
  Header, Left, Body, Right , Title, Content, 
  ListItem, List, Icon, Container, Grid, Row, Col,
  Text, Button, Thumbnail,
} from 'native-base';
import {changeNomer } from '@Apis'
import { resetNavigation } from '@AuthCheck';
import LinearGradient from 'react-native-linear-gradient';
import {mocks} from '../../../constants/index';
import {connect} from 'react-redux';
import {DELETE_ITEM} from '@redux_store';



const {width} = Dimensions.get('window');

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      modal :false,
      changeNumberPhone : ""
    }
    this.settingList = mocks.settingList;
    StatusBar.setBarStyle( 'light-content',true)
    StatusBar.setBackgroundColor("#64e291")
  }

  folowonme() {
    return (
      <View style={{flex:1, flexDirection:'column'}}>
        {this.settingList.map((l, i) => (
          <View key={i}  style={{alignItems:'center'}}>
            <Button 
              onPress={() => Linking.openURL(l.urlsite)}
              key={{backgroundColor:'transparent'}}>
              <Image source={l.avatar_url} style={{height:50, width:50}}/>
            </Button>
          </View>
        ))}
      </View>
    )
  }


  async componentDidMount() {

      const data_profile = await AsyncStorage.getItem('Profile');
      const prhone       = await AsyncStorage.getItem('nomer');
      var data = JSON.parse(data_profile);
      if(data !== null) {
        this.setState({
          profile : data,
          changeNumberPhone : prhone
        });
      } 

  }


  logout() {
    Alert.alert(
      'Keluar?',
      'anda masih akan bisa login dengan nik dan password',
      [
        { text: 'Cancel'},
        {text: 'OK', onPress: () => {
          
          this.props.del_session_redux();

          AsyncStorage.removeItem('token');
          AsyncStorage.removeItem("Profile");
          AsyncStorage.removeItem("nomer");

          resetNavigation('Login').then((res) => {
            this.props.navigation.dispatch(res);
          })
        }},
      ],
      {cancelable: true},
    );
  }
  handleChangeNo() {
    if(this.state.changeNumberPhone.length != 12) {
        Alert.alert('Error', 'Nomor harus 12 huruf');
    } else {
      /* porses to call api here */
      changeNomer({nomer : this.state.changeNumberPhone}).then(async res => {
        if(res.data.code == 1) {
         
          this.setState({modal: false});
          this.setState({...this.state, profile : { phone : this.state.changeNumberPhone}});
          ToastAndroid.show("nomer sudah diperbarui", ToastAndroid.SHORT);
          AsyncStorage.setItem('nomer', this.state.changeNumberPhone);
        }
      });
    }
  }
  render() {

    return (
      <Container>
          <Header transparent style={{backgroundColor:'#64e291', zIndex:100000}}>
              <Left/>
              <Body>
                  {/* <Title bold size={18}>Profile </Title>/ */}
              </Body>
              <Right>
                <Button transparent
                style={{backgroundColor : 'transparent', zIndex:10000000}}
                onPress={() => {
                  this.setState({
                    modal :!this.state.modal,
                    changeNumberPhone : this.state.profile.phone
                  })
                }}
              >
                <Icon name="add" size={24}/>
              </Button>
              </Right>
            </Header>
            <Content>
              <ScrollView showsVerticalScrollIndicator={false} style={{}}>
              <LinearGradient colors={['#64e291', '#00BF4A']} style={{zIndex:1000001}}>
                  <Grid style={styles.statisticwarga}>
                    <Row>
                      <Col>
                        <View style={{alignItems:'center', justifyContent:'center'}}>
                          <Thumbnail
                              large
                              square
                              source={
                                allLogo.kuser
                              }
                              style={styles.avatar}
                            />
                          <Text  style={{alignItems:'center',textWeigted:'small', marginVertical:15, textAlign:'center', fontSize:20, color:'#fff', marginVertical:3}} >{this.state.profile.username}</Text>
                          <Text style={[styles.contentProfileText, {alignItems:'center', textAlign:'center', color:'#fff', marginTop:-10}]}>RT {this.state.profile.rt} | RW {this.state.profile.rw}</Text>
                        </View>
                      </Col>
                    </Row>
                  </Grid>
              </LinearGradient> 
              <View style={{backgroundColor:'#fff', zIndex:1000, marginTop:10}}>
                <View style={{borderColor:'#eee',borderRadius:10, borderWidth:2, marginHorizontal:10, marginTop:10}}> 
                  <Grid>
                    <Row>
                      <Col>
                          <Text style={[styles.contentProfileText, {marginHorizontal:15}]}>Nik</Text>
                          <Text style={[styles.contentProfileText, {marginHorizontal:15}]}>No hp</Text>
                      </Col>
                      <Col>
                          <Text style={[styles.contentProfileText,{textAlign:'right', marginHorizontal:15}]}>{this.state.profile.nik}</Text>
                          <Text style={[styles.contentProfileText,{textAlign:'right',  marginHorizontal:15}]}>{this.state.profile.phone}</Text>
                      </Col>
                    </Row>
                  </Grid>
                </View>
          
                <Content style={{marginTop:20}} enableOnAndroid>
                  <List>
                    <ListItem itemDivider>
                      <Text>Tentang kami</Text>
                    </ListItem >
                    { this.settingList.map((l, i) => (
                      <ListItem avatar onPress={() => Linking.openURL(l.urlsite)} key={i}>
                        <Left>
                          <Image source={l.avatar_url} style={{height:15, width:15}}/>
                        </Left>
                        <Body>
                          <Text style={{color:'#000', fontWeight:'small', fontSize:14}}>{l.name}</Text>
                        </Body>
                        <Right>
                          <Icon name="arrow-forward" />
                        </Right>
                      </ListItem>
                      ))}
                      <ListItem itemDivider >
                        <Text>Info aplikasi</Text>
                      </ListItem>
                      <ListItem icon onPress={() => this.props.navigation.navigate('Info_app')}>
                        <Left>
                            <Icon name="md-information-circle" />
                          </Left>
                          <Body>
                            <Text style={{color:'#000', fontWeight:'small', fontSize:14}}>Info aplikasi</Text>
                          </Body>
                          <Right>
                            <Icon name="arrow-forward" />
                          </Right>
                      </ListItem>
                      <ListItem icon onPress={() =>Linking.openURL(mocks.infoApp.url_ratting)}>
                        <Left>
                          <Icon name="star" />
                        </Left>
                        <Body>
                          <Text style={{color:'#000', fontWeight:'small', fontSize:14}}>Beri Penilaian</Text>
                        </Body>
                        <Right>
                          <Icon name="arrow-forward" />
                        </Right>
                      </ListItem>
                      <ListItem itemDivider style={{marginTop:20, backgroundColor:'#eee'}}>
                        <Left>
                          <Text bold>Versi applikasi</Text>
                        </Left>
                        <Right>
                          <Text>{mocks.infoApp.version}</Text>
                        </Right>
                      </ListItem>
                  </List>
                </Content>
              </View>
              <View>
                <Button title="keluar" onPress={() => this.logout() } block
                style={{backgroundColor:'red', flex:1, alignItems:'center', marginBottom:60, borderRadius:0}}> 
                  <Text white>Keluar</Text>
                </Button>
              </View>
            </ScrollView>
            <Modal
            visible={this.state.modal}
            onRequestClose={() => this.setState({modal:false})}
            animationType="slide"
            transparent={true}
            closeOnClick={true}
          >
            <View>
              <TouchableWithoutFeedback 
              onPress={() => this.setState({modal : !this.state.modal})}
              >
                <View style={styles.modalConatiner}>
                  <View style={styles.content_model}>
                      <Text h2>Informasi akun</Text>
                      <View>
                        <KeyboardAvoidingView behavior='padding'>
                          <Text>Nomer</Text>
                          <TextInput
                            onChangeText={(text)=> this.setState({changeNumberPhone: text})}
                            value={this.state.changeNumberPhone}
                            keyboardType="phone-pad"
                            autoFocus={true}
                            maxLength={12}
                            style={{padding:10, borderColor:'#eee', borderWidth:1, color:'#000'}}
                          />
                         
                          <Button block success style={{marginTop:20}}
                          onPress={() => this.handleChangeNo()}  
                        >
                          <Text white style={{alignItems:'center', justifyContent:'center', padding:10, }}>Simpan</Text>
                        </Button>
                        </KeyboardAvoidingView>
                        
                      </View>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </Modal>
          </Content>
      </Container>
    )
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
      del_session_redux : () => dispatch({type : DELETE_ITEM})
  }
}

export default connect(null, mapDispatchToProps)(withNavigation(Settings));

const styles = StyleSheet.create({
  statisticwarga : {
    flex:1,
    height : 200,
    width : width,
    
  },
  containerProfile : {
    paddingVertical: 20,
    flex: 1,
    flexDirection : 'row',
    paddingHorizontal : 20,


  },
  modalConatiner : {
    backgroundColor:'transparent',
    width:'100%',
    height: '100%',
  },  
  content_model : {
    paddingHorizontal: 20,
    height:'100%',
    paddingVertical:10,
    marginTop:200,
    backgroundColor:'#fff',
  },
   contenProfile : {
    textAlign:'center',
    justifyContent: 'space-between'

   },
   profile_info :{
     flex: 1, 
     justifyContent: 'space-between', 
     alignItems:'center', 
     flexDirection: 'row',  
     paddingHorizontal: 0, 
     paddingVertical:10
  },
  
  contentProfileText : {
     paddingVertical : 10,
     fontSize : 14,
   },

  containterAvatar : {
    justifyContent : 'center',
    alignItems : 'center',

  },  
  avatar: {
    borderColor:'#fff', 
    borderRadius: 10, 
    borderWidth:1,
    marginTop:20,
    height: 100,
    width: 100,
    zIndex:1000,
  },


})
