import React, { 
  Component 
} from 'react'
import { 
  StatusBar,
  Alert,

  AsyncStorage
} from 'react-native'
import { Container, Toast} from 'native-base';
import { FooterTabHome } from '../../components';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Home from './Home';
import Logs from './Logs';
import Settings from './More';
import Warga from './Warga';
import OneSignal from 'react-native-onesignal';
import { resetNavigation } from '@AuthCheck';
import {surat_analysis} from '@Apis';
import NetInfo from "@react-native-community/netinfo";

export default class Layout extends Component {
    constructor(props) {
        console.disableYellowBox =true
        super(props); 
        this.state = {
           FooterTab:1,
           activeTab : null,
        }
    }

    componentWillMount() {
      OneSignal.init("dd5cf332-2c70-4a3c-a20c-b9b8e4a7867a");
      OneSignal.addEventListener('received', this.openNotif.bind(this));
      OneSignal.enableSound(true);
      OneSignal.enableVibrate(true);
      OneSignal.inFocusDisplaying(2);
    }

    componentDidMount() {
      StatusBar.setBarStyle( 'light-content',true);
      StatusBar.setBackgroundColor("#64e291");

      surat_analysis().then((res) => {
        if(!res.data.autenticated) {
          Alert.alert("Login diperlukan","Login Untuk melanjutkan kembali", [
              {text: 'OK', onPress: () => {
                resetNavigation('Login').then((res) => {
                  AsyncStorage.removeItem('token');
                  this.props.navigation.dispatch(res);
                })
              }},
            ],
            {cancelable: false},
          )
            
        } else {
          NetInfo.fetch().then(res => {
            if(!res.isConnected) {
              Alert.alert(
                'tidak ada koneksi','perikasa koneksi internet anda '
              )
            }
          });
        }
      })
     
    }
    openNotif(data) {
      Toast.show({
        type : 'success',
        duration : 3000, 
        buttonText : 'oke',
        text : data.payload.body
      })
    }

    componentWillUnmount() {
      OneSignal.removeEventListener('received', this.openNotif.bind(this));
      
    }
    dataCallback = (activeTab)  => {
      this.setState({ FooterTab: 2, activeTab : activeTab});
    }

    move_menu(number) {
        let result = undefined;
       switch(number) {
           case 1 : 
                result = <Home dataClickFromCild={this.dataCallback}/>;
                break;
            case 2 : 
              result = <Logs active={this.state.activeTab}/>;
              break;
            case 3 :
              result = <Warga/>;
                break;
            case 4 : 
                result = <Settings/>;
                break;
            default :
                result= <Home/>;
                break;
       }
       return (result);
    }


  changeTab = (number) => {
    if (this.state.FooterTab !== number) {
      this.setState({ FooterTab: number, activeTab : 0 });
    }
  }

  render() {
    return (
        <Container>
              {this.move_menu(this.state.FooterTab)}
              <FooterTabHome 
                homActive={this.state.FooterTab === 1}
                homPress={() => this.changeTab(1)}
                hisActive={this.state.FooterTab === 2}
                historyPress={() => this.changeTab(2)}
                wargaActive={this.state.FooterTab === 3}
                wargaPrss={() => this.changeTab(3)}
                settingActive={this.state.FooterTab === 4}
                logsPress={() => this.changeTab(4)}
              />
        </Container>
    )
  }
}