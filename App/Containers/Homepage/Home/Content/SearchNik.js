import React, {Component} from 'react';
import { 
    Header, 
    Text,
    Item, 
    Input, 
    Icon, 
    Content, 
    Spinner,
    List, ListItem, Left, Body, Right, Thumbnail, Toast, Container,
} from 'native-base';
import {
    View,
    StyleSheet,
    Alert,
    Image,
    StatusBar
} from 'react-native';

import { allLogo } from '@assets'
import {choseActionNew} from '@AuthCheck';
import {get_warga} from '@Apis';
import { withNavigation } from 'react-navigation';

class SearchNik extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search : '',
            data : [], 
            onpreSearch : false,
            showLoading : true
        }
        this.type = this.props.navigation.getParam('type');
    }

    componentDidMount() {
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#64e291");
        get_warga().then((res => {
            if(Array.isArray(res.data.data)) {
                this.setState({
                    data : res.data.data,
                    showLoading: false,
                });
            } else {
                Alert.alert('Tidak bisa terhubung ke jaringan');    
            }
        })).catch(err=> {
            Alert.alert('Tidak bisa terhubung ke jaringan', err);
        })
    }

    handleupdate = async(val) =>{
        this.setState({search : val.toLowerCase()});
        this.setState({showLoading : true});
        get_warga({keywords:val}).then((res) => {
            if(Array.isArray(res.data.data)) {
                this.setState({
                    data : res.data.data,
                    showLoading: false,
                });
            } else {
                this.setState({
                    data : [],
                    showLoading: false
                });
            }
        });
    }
  
    render() {
        return (
            <Container>
                <Header searchBar rounded style={{backgroundColor:'#64e291', zIndex:100000}}>
                    <Item>
                        <Icon name="ios-search" />
                        <Input 
                            placeholder="Cari warga"
                            autoFocus={true}
                            autoCorrect={false}
                            value = {this.state.search}
                            onChangeText={(val) => this.handleupdate(val)}
                            onClear= {() => this.handleupdate('')}
                        />
                        <Icon name="ios-people" />
                    </Item>
                </Header>
                <Content>
                    <List>
                    {   this.state.showLoading != true ? 
                            this.state.data.length > 0 ? 
                                this.state.data.map((item, key) => {
                                    return (
                                        <ListItem avatar button={true} onPress={() => this.buat_surat(item.id)}>
                                            <Left>
                                                <Thumbnail source={allLogo.kuser} style={{width:40, height:40}}  />
                                            </Left>
                                            <Body>
                                                <Text>{item.nama}</Text>
                                                <Text note>{item.nik}</Text>
                                            </Body>
                                            <Right>
                                                <Icon name="arrow-forward" />
                                            </Right>
                                        </ListItem>
                                    )
                                })
                            :  
                            <View style={{flex:1, flexDirection:'column', alignItems:'center', justifyContent:'center',  marginTop: 130 }}>
                                <Image source={allLogo.notFound}  style={{width:250, height:200}}/>
                                <Text style={{marginTop:-30}}>data tidak ditemukan</Text>
                            </View> 
                    : 
                    <View style={{flex:1, flexDirection:'column', alignItems:'center', justifyContent:'center',  marginTop: 130 }}>
                        <Spinner/>
                        <Text h2 style={{marginTop:5}}>Mohon tunggu...</Text>
                    </View> 
                    }
                    </List>
                </Content>
            </Container>
        );
    }
    async buat_surat(id_warga) {
        try { 
            const id_surat = await this.props.navigation.getParam('id_surat');
            const aksi     = await choseActionNew(id_surat);
            await this.props.navigation.navigate( aksi, {
                    id_surat : id_surat,
                    id_warga : id_warga
            });

        } catch(er) {
            Toast.show({
                type:'warning',
                text:'kealahan terjadi tanpa terdudaga',
                buttonText : 'ok',
                duration: 3000,
            })
        }
    }
}


export default withNavigation(SearchNik);

const stylese = StyleSheet.create({
    container: {
        flex: 1,
      },
    
    header : {
        backgroundColor:'#33B77B',
        maxHeight:80,
    },
    caption : {
        flex:1, 
        flexDirection:'column',
        maxHeight:30,
        marginHorizontal:20

    },
    inputStyle : {
        backgroundColor: '#aaa',
        marginVertical : 20,
        justifyContent:'flex-end'
        
    },
    buttonStyle : {
        position:'absolute', 
        top:0, 
        left:0, 
        
    }
});

