import React,{Component} from 'react';
import {
    Alert,
    StyleSheet,
    StatusBar,
    Image,
    TouchableHighlight
} from 'react-native';
import { allLogo } from '@assets'
import config from '../../../../../Configs/config';
import { new_pengantar_surat, warga_description } from '@Apis';
import ImagePicker from 'react-native-image-picker'
import LottieView from 'lottie-react-native';
import { withNavigation } from 'react-navigation';
import { 
    Header, 
    Text,
    Title,
    Icon, 
    Content, 
    Button,
    Toast,
    Row,
    Col,
    Item,
    Left, Body, Right, Container, Form,Textarea, Grid, Input, Label, View,
} from 'native-base';
import {uploadtoServer} from '../../../../../Helper/uploadImage';
import {Process, Done} from '../../../../../components';
import {connect} from 'react-redux';
import { ADD_PENDING } from '@redux_store';

class General extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataWarga : { nama : '', nik:'', tanggallahir : '', tempatlahir:''},
            jenisSurat : '',
            loading : false,
            perihal : '',
            success : false,
            images_kk:'',
            images_ktp : '',
            keterangan : "",
            kirim : true,

        };
    }
     async componentDidMount() {
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#64e291");
        const { navigation } = this.props;
        const id_warga      =  await navigation.getParam('id_warga');
        const id_surat      =  await navigation.getParam('id_surat');

        warga_description(id_warga).then(async (res) => {

            console.log(res.data);
            this.setState({
                dataWarga  : res.data.data,
                jenisSurat : id_surat,
                keterangan : 'ingin membuat surat pengantar'
            });
        });
        
    }
  
    uploadImage(type) {
        const options = {
            quality: 1.0,
            storageOptions: {
              skipBackup: true
            }
        };


        Alert.alert(
            'Tambah Kelengkapan',
            'Silahkan pilih sumber',
            [
                {text: 'Camera', onPress: () => {
                    ImagePicker.launchCamera(options, (res) => {
                        let source = {
                            uri : res.uri,
                            fileName : res.fileName,
                            fileSize : res.fileSize,
                            type : res.type
                        };
                        if (type == 'kk') this.setState({images_kk : source}) 
                        else this.setState({images_ktp : source})
                        
                    });
                }},
                {text: 'Gallery',onPress: () => {
                    ImagePicker.launchImageLibrary(options, (res) => {
                        let source = {
                            uri : res.uri,
                            fileName : res.fileName,
                            fileSize : res.fileSize,
                            type : res.type
                        };

                        if (type == 'kk') this.setState({images_kk : source}) 
                        else this.setState({images_ktp : source})
                    });
                }},
            ],
            {cancelable: true},
        )
       
    } 

    async upload() {
        
        let form        = await new FormData();
        form.append('foto_kk', { 
            'uri' : this.state.images_kk.uri,
            'name' : this.state.images_kk.fileName,
            'type' : this.state.images_kk.type
        });

        form.append('foto_ktp', { 
            'uri' : this.state.images_ktp.uri,
            'name' : this.state.images_ktp.fileName,
            'type' : this.state.images_ktp.type
        });

        /* send to servers */
        let data = await uploadtoServer(form);
        return data;
      
    }

    async kirimPengantar() {
        this.setState({loading: true, kirim : false});
        let {keterangan, perihal} = this.state;
        const data_uri = await this.upload(); 
        
        const path_url_image = config.url_assets + '/assets/files/dokumen/';
        if(data_uri) {
            if(keterangan && perihal) {
                let data = {
                     'pbb' : path_url_image + data_uri.image_pbb,
                     'kk'  : path_url_image + data_uri.image_kk,
                     'perihal'    : perihal,
                     'keterangan' : keterangan,
                     'id_penduduk'    : this.state.dataWarga.id,
                     'id_jenissurat'  : this.state.jenisSurat,
                     'id_rt'         : this.state.dataWarga.id_pend_rt,
     
                };
                /* send data rt to server */
                
                    new_pengantar_surat(data).then((res) =>{
                        if(res.data.code == 0) {
                            this.setState({loading: false, success: true});
                            this.props.add_pending();
                            setTimeout(() => {
                                this.animation.play();
                            },500);
                            setTimeout(() => {
                                this.setState({success: false, perihal:''});
                                this.props.navigation.navigate('Home');
                            },3000);
                        } else {
                            this.setState({loading: false, success: false});
                            Toast.show({    
                                text: "server internal error",
                                buttonText: "Oke",
                                type: "danger",
                                overlay : '0.5'
                            });
                        }
                    }); 
                
            } else {
                this.setState({loading: false, kirim : true});
                Toast.show({
                    text: "harap input semua fild",
                    buttonText: "Oke",
                    type: "danger",
                    overlay : '0.5'
                });
            }
        } else {
            this.setState({loading: false, kirim:true});
            Toast.show({
                text: "gagal saat proses kirim data",
                buttonText: "Oke",
                type: "danger",
                overlay : '0.5'
            });
        }
    }

    render() {
        return (
            <Container>
                <Header  style={{backgroundColor:'#64e291'}}>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name='arrow-back' />
                    </Button>
                </Left>
              <Body>
                  <Title>Pengantar Umum</Title>
              </Body>
              <Right/>
              
            </Header>
            <Content>

                <Form style={{marginHorizontal:20, marginTop:20}}>
                    <Grid>        
                        <Row style={{marginTop:10}}>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Pemohon</Label>
                                    <Input disabled value={this.state.dataWarga.nama} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <Item stackedLabel> 
                                            <Label>Agama</Label>
                                            <Input disabled value={this.state.dataWarga.agama} style={styles.text_info}/>
                                        </Item>
                                    </Col>
                                    <Row>
                                        <Col>
                                            <Item stackedLabel> 
                                                <Label>Jenis Kelamin</Label>
                                                <Input disabled value={this.state.dataWarga.gender} style={styles.text_info}/>
                                            </Item>
                                        </Col>
                                    </Row>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Nik</Label>
                                    <Input disabled value={this.state.dataWarga.nik} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tanggal Lahir</Label>
                                    <Input disabled value={this.state.dataWarga.tanggallahir} style={styles.text_info}/>
                                </Item>
                            </Col>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tempat Lahir</Label>
                                    <Input underline value={this.state.dataWarga.tempatlahir} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Nama Rt</Label>
                                    <Input underline value={this.state.dataWarga.nama_rt} disabled style={styles.text_info} />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Rt</Label>
                                    <Input underline disabled value={this.state.dataWarga.rt} style={styles.text_info}/>
                                </Item>
                            </Col>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Rw</Label>
                                    <Input underline disabled value={this.state.dataWarga.rw} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Perihal *</Label>
                                    <Input underline style={styles.text_info}  autoCapitalize={false} value={this.state.perihal} autoCorrect={false} onChangeText={(val) => this.setState({'perihal' : val})}/>
                                </Item>
                            </Col>
                        </Row>
                      
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Keterangan</Label>
                                    <Textarea autoCorrect={false}  style={styles.text_info} rowSpan={3} style={{width:'100%'}} value={this.state.keterangan} onChangeText={(text) => this.setState({keterangan: text})} ></Textarea>
                                </Item>
                            </Col>
                        </Row>
                        <Row style={{marginBottom:20}}>
                            <Col >
                            {this.state.images_ktp == ''? 
                                <Button block transparent onPress={() => this.uploadImage('ktp')}>
                                    <Text style=    {{alignItems:'center', color:'#aaa', fontWeight:'bold'}}> Tambah Foto KTP</Text>
                                </Button>
                            : 
                                <View>
                                    <TouchableHighlight onPress={() => this.uploadImage('ktp')} style={{marginTop:20}}>
                                        <Image source={{uri:this.state.images_ktp.uri}} style={{width:'100%', height:200}} resizeMode="cover" />
                                    </TouchableHighlight>
                                    { this.state.images_kk == ''?
                                        <Button block transparent onPress={() => this.uploadImage('kk')}>
                                            <Text style=    {{alignItems:'center', color:'#aaa', fontWeight:'bold'}}> Tambah Foto KK</Text>
                                        </Button>
                                    : 
                                    <View>
                                        
                                        <TouchableHighlight onPress={() => this.uploadImage('kk')} style={{marginTop:20}}>
                                            <Image source={{uri:this.state.images_kk.uri}} style={{width:'100%', height:200}} resizeMode="cover"/>
                                        </TouchableHighlight>
                                       {
                                           this.state.kirim  ?
                                                <Button block success onPress={() => this.kirimPengantar()} style={{marginTop:20, marginBottom:20}}>
                                                    <Text style={{alignItems:'center', color:'#FFF', fontWeight:'bold'}}>KIRIM DATA</Text>
                                                </Button>
                                           : null
                                       }
                                    </View>
                                    }
                                </View>

                            } 
                            </Col>
                        </Row>
                        
                    </Grid>
                </Form>
            </Content>

            {this.state.loading ? 
                <View style={styles.notif}>
                    <Process/>
                </View>
            : null
            }
                {this.state.success ? 
                   <View style={styles.notif}>
                        <Done style={{}}>
                            <LottieView
                                ref={animation => {
                                this.animation = animation;
                                }}
                                source={allLogo.success}
                                loop={false}
                                style={styles.lotie}
                            />
                            <Text h2 style={styles.text}>Permhohonan terkirim</Text>
                        </Done>
                    </View>
                : null
                } 
        </Container>
        )
    }

    
}


const mapDispatchToProps = (dispatch) => {
    return {
        add_pending : () => dispatch({type : ADD_PENDING})
    }
}
export default connect(null, mapDispatchToProps)(withNavigation(General));


const styles = StyleSheet.create({
    text_info : {
      fontSize: 14
    },

    notif : {
        zIndex:1000001, 
        position:'absolute', 
        left:0, 
        right:0, 
        top:180, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    done : {
        zIndex:1000001, 
        position:'absolute',

        justifyContent: 'center', 
        alignItems: 'center'
    },
    lotie : {
        width: 150,
        height: 150,
        justifyContent:'center',
        alignItems:'center'
    },
    text : {
        marginTop:-40,
        justifyContent:'center', 
        alignItems:'center', 
        textAlign:'center', 
        paddingBottom:20,
        color:'#000'
    },
})