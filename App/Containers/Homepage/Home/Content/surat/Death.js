import React,{Component} from 'react';
import {
    Alert,
    StyleSheet,
    StatusBar,
    Image,
    TimePickerAndroid,
    TouchableHighlight
} from 'react-native';
import { allLogo } from '@assets'
import config from '../../../../../Configs/config';
import { new_pengantar_surat, warga_description, get_warga } from '@Apis';
import ImagePicker from 'react-native-image-picker'
import { withNavigation } from 'react-navigation';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import { 
    Header, 
    Title,
    Icon, 
    Content, 
    Button,
    Toast,
    Row,
    List,
    Col,
    Item,
    Picker,
    DatePicker,
    Text,
    Left, Body, Right, Container, Form,Textarea, Grid, Input, Label, View, ListItem,
} from 'native-base';
import {uploadtoServer} from '../../../../../Helper/uploadImage';
import {Process, Done} from '../../../../../components';
import {connect} from 'react-redux';
import { ADD_PENDING } from '@redux_store';


class Death extends Component {
    constructor(props) {
        super(props);
        this.setDate = this.setDate.bind(this);
        this.state = {
            dataWarga : { nama : '', nik:'', tanggallahir : '', tempatlahir:''},
            jenisSurat : '',
            loading : false,
            success : '',
            images_kk:'',
            images_ktp : '',
            keterangan : "",
            data_pelapor : [],
            search : '',
            id_pelapor : "",
            nama_pelapor : "",
            sebab: '',
            tanggal : new Date(),
            jam : '',
            hubungan : '',
            tempat : '',
            selected : 'in', 
            kirim : true,


        };
    }
    setDate(date) {
        let tgl_meninggal = moment(date).format('YYYY-MM-DD');
        this.setState({tgl_mati : tgl_meninggal});
    }

     async componentDidMount() {
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#64e291");
        const { navigation } = this.props;
        const id_warga      =  await navigation.getParam('id_warga');
        const id_surat      =  await navigation.getParam('id_surat');

        warga_description(id_warga).then(async (res) => {
            this.setState({
                dataWarga  : res.data.data,
                jenisSurat : id_surat,
                keterangan : 'ingin membuat surat Pengantar Kematian',
                warga_pelapor : true // jika true maka dalam desa jika false maka luar kota
            });
        });
        
    }
  
    uploadImage(type) {
        const options = {
            quality: 1.0,
            storageOptions: {
              skipBackup: true
            }
        };


        Alert.alert(
            'Tambah Kelengkapan',
            'Silahkan pilih sumber gambar',
            [
                {text: 'Camera', onPress: () => {
                    ImagePicker.launchCamera(options, (res) => {
                        let source = {
                            uri : res.uri,
                            fileName : res.fileName,
                            fileSize : res.fileSize,
                            type : res.type
                        };
                        if (type == 'kk') this.setState({images_kk : source}) 
                        else this.setState({images_ktp : source})
                        
                    });
                }},
                {text: 'Gallery',onPress: () => {
                    ImagePicker.launchImageLibrary(options, (res) => {
                        let source = {
                            uri : res.uri,
                            fileName : res.fileName,
                            fileSize : res.fileSize,
                            type : res.type
                        };

                        if (type == 'kk') this.setState({images_kk : source}) 
                        else this.setState({images_ktp : source})
                    });
                }},
            ],
            {cancelable: true},
        )
       
    } 

    async upload() {
        
        let form        = await new FormData();
        form.append('foto_kk', { 
            'uri' : this.state.images_kk.uri,
            'name' : this.state.images_kk.fileName,
            'type' : this.state.images_kk.type
        });

        form.append('foto_ktp', { 
            'uri' : this.state.images_ktp.uri,
            'name' : this.state.images_ktp.fileName,
            'type' : this.state.images_ktp.type
        });

        /* send to servers */
        let data = await uploadtoServer(form);
        return data;
      
    }

    async kirimPengantar() {
        this.setState({loading: true, kirim:false});
        let {keterangan} = this.state;
        const data_uri = await this.upload();
        
        const path_url_image = config.url_assets + '/assets/files/dokumen/';
        if(data_uri) {
            if(keterangan) {
                let data = {
                     'pbb' : path_url_image + data_uri.image_pbb,
                     'kk'  : path_url_image + data_uri.image_kk,
                     'keterangan' : keterangan,
                     'id_penduduk' :  this.state.dataWarga.id,
                     'id_jenissurat'  : this.state.jenisSurat,
                     'nama_pelapor' : this.state.selected == 'in' ? '' : this.state.nama_pelapor,
                     'id_pelapor' : this.state.id_pelapor,
                     'sebab' : this.state.sebab,
                     'jam' : this.state.jam,
                     'tgl_mati' : this.state.tanggal,
                     "hubungan" : this.state.hubungan,
                     'tempat' : this.state.tempat,
                     'selected' : this.state.selected,

     
                };
                new_pengantar_surat(data).then((res) =>{
                    if(res.data.code == 0)    {
                        this.props.add_pending();
                        this.setState({success: true, loading:false});
                        setTimeout(() => {
                            this.animation.play();
                        },500);
                        setTimeout(() => {
                            this.props.navigation.navigate('Home');
                        },2000);
    
                    } else {
                        this.setState({loading: false, kirim: true});
                        Toast.show({    
                            text: "server internal error",
                            buttonText: "Oke",
                            type: "danger",
                            overlay : '0.5'
                        });
                    }
                }); 
                
            } else {
                Toast.show({
                    text: "harap input semua fild",
                    buttonText: "Oke",
                    type: "danger",
                    overlay : '0.5'
                });
            }
        } else {
            Toast.show({
                text: "gagal saat proses kirim data",
                buttonText: "Oke",
                type: "danger",
                overlay : '0.5'
            });
        }
    }
    onValueChange(value) {
        this.setState({
          selected: value
        });
    }

    list_nama_pelapor() {
        return(
            <List style={{backgroundColor:'#eee',zIndex:1000001}}>
            {
                this.state.data_pelapor.length > 0 ? 
                this.state.data_pelapor.map((item, key) => {
                    return (
                        <ListItem button={true} onPress={() => this.choice_pelapor_id(item.id, item.nama)}>
                            <Text>{item.nama}</Text>
                        </ListItem>
                    )}
                )
                :  
                null  
            }
            </List>
        );
    }

    handleupdate(val){
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#75D463")

        this.setState({search : val.toLowerCase()});
        get_warga({keywords:val}).then((res) => {
            if(Array.isArray(res.data.data)) {
                this.setState({
                    data_pelapor : res.data.data.length > 5 ? res.data.data.splite(0,3) : res.data.data,
                });
            } else {
                Toast.show({
                    text: "Data tidak ditemukan",
                    buttonText: "Oke",
                    type: "danger",
                    position:'top',
                    overlay : '0.5'
                });
            }
        });
    }

    choice_pelapor_id(id, nama) {
        this.setState(
            {
                id_pelapor : id, data_pelapor: [],
                search : nama,
            }
        );

    }
   
    render() {
        return (
            <Container>
                <Header  style={{backgroundColor:'#64e291'}}>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name='arrow-back' />
                    </Button>
                </Left>
              <Body>
                  <Title>Pengantar Kematian</Title>
              </Body>
              <Right/>
              
            </Header>
            <Content>
                <Form style={{marginHorizontal:20, marginTop:20}}>
                    <Grid>                        
                        <Row style={{marginTop:10}}>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Pemohon</Label>
                                    <Input disabled value={this.state.dataWarga.nama} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <Item stackedLabel> 
                                            <Label>Agama</Label>
                                            <Input disabled value={this.state.dataWarga.agama} style={styles.text_info}/>
                                        </Item>
                                    </Col>
                                    <Row>
                                        <Col>
                                            <Item stackedLabel> 
                                                <Label>Jenis Kelamin</Label>
                                                <Input disabled value={this.state.dataWarga.gender} style={styles.text_info}/>
                                            </Item>
                                        </Col>
                                    </Row>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Nik</Label>
                                    <Input disabled value={this.state.dataWarga.nik} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tanggal Lahir</Label>
                                    <Input disabled value={this.state.dataWarga.tanggallahir} style={styles.text_info}/>
                                </Item>
                            </Col>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tempat Lahir</Label>
                                    <Input underline value={this.state.dataWarga.tempatlahir} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Nama Rt</Label>
                                    <Input underline value={this.state.dataWarga.nama_rt} disabled style={styles.text_info} />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Rt</Label>
                                    <Input underline disabled value={this.state.dataWarga.rt} style={styles.text_info}/>
                                </Item>
                            </Col>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Rw</Label>
                                    <Input underline disabled value={this.state.dataWarga.rw} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                           <Col>
                                <Item stackedLabel>
                                    <Label>Pelapor Dalam/Luar Desa</Label>
                                    <Row style={styles.inOut}>
                                       
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name="arrow-down" />}
                                            headerStyle={{ backgroundColor: "#b95dd3" }}
                                            headerBackButtonTextStyle={{ color: "#fff" }}
                                            headerTitleStyle={{ color: "#fff" }}
                                            selectedValue={this.state.selected}
                                            onValueChange={this.onValueChange.bind(this)}
                                            >
                                            <Picker.Item label="Warga Dalam" value="    " />
                                            <Picker.Item label="Warga Luar" value="out" />
                                        </Picker>
                                    </Row>
                                </Item>
                           </Col>
                        </Row>
                        <Row>
                            <Col>
                            { this.state.selected == 'in' ? 
                                <View style={{marginHorizontal:10}}>
                                    <View>
                                        {this.list_nama_pelapor() }
                                    </View>
                                    <Item stackedLabel> 
                                        <Label>Cari Nama Pelapor*</Label>
                                        <Input 
                                            underline 
                                            style={styles.text_info}  
                                            autoCapitalize={false} 
                                            value={this.state.search} 
                                            autoCorrect={false} 
                                            onChangeText={(val) => this.handleupdate(val)}
                                            onClear= {() => this.handleupdate('')}
                                            />
                                    </Item> 
                                </View>
                            : 
                            <View>
                                <Row>
                                    <Col style={styles.content_luar_desa}>
                                        <Item stackedLabel> 
                                            <Label>Nama Pelapor *</Label>
                                            <Input underline style={styles.text_info}  autoCapitalize={false} value={this.state.perihal} autoCorrect={false} onChangeText={(val) => this.setState({nama_pelapor : val})}/>
                                        </Item>
                                    </Col>
                                </Row>
                            </View>
                            }
                            </Col>    
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Hubungan Pelapor</Label>
                                    <Input underline style={styles.text_info}  autoCapitalize={false} value={this.state.hubungan} autoCorrect={false} onChangeText={(val) => this.setState({'hubungan' : val})}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Pilih Tanggal</Label>
                                    <DatePicker
                                        style={{alignItems:'flex-start', width:'100%'}}
                                        defaultDate={new Date()}
                                        minimumDate={new Date(2000, 1, 1)}
                                        maximumDate={new Date()}
                                        locale={"id"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText="Piih Tanggal kematian"
                                        textStyle={{ color: "#aaa" }}
                                        placeHolderTextStyle={{ color: "#d3d3d3" }}
                                        onDateChange={this.setDate}
                                        disabled={false}
                                        />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Jam Meningal *</Label>
                                    <Input onFocus={() => this.select_jam()} underline style={styles.text_info}  autoCapitalize={false} value={this.state.jam} autoCorrect={false} />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Penyebab Kematian</Label>
                                    <Textarea autoCorrect={false}  style={styles.text_info} rowSpan={3} style={{width:'100%'}} value={this.state.sebab} onChangeText={(text) => this.setState({sebab: text})} ></Textarea>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tempat Kematian</Label>
                                    <Textarea autoCorrect={false}  style={styles.text_info} rowSpan={3} style={{width:'100%'}} value={this.state.tempat} onChangeText={(text) => this.setState({tempat: text})} ></Textarea>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Keterangan</Label>
                                    <Textarea autoCorrect={false}  style={styles.text_info} rowSpan={3} style={{width:'100%'}} value={this.state.keterangan} onChangeText={(text) => this.setState({keterangan: text})} ></Textarea>
                                </Item>
                            </Col>
                        </Row>
                        <Row style={{marginBottom:20}}>
                            <Col >
                            {this.state.images_ktp == ''? 
                                <Button block transparent onPress={() => this.uploadImage('ktp')}>
                                    <Text style=    {{alignItems:'center', color:'#aaa', fontWeight:'bold'}}> Tambah Foto KTP</Text>
                                </Button>
                            : 
                                <View>
                                    <TouchableHighlight onPress={() => this.uploadImage('ktp')} style={{marginTop:20}}>
                                        <Image source={{uri:this.state.images_ktp.uri}} style={{width:'100%', height:200}}/>
                                    </TouchableHighlight>
                                    { this.state.images_kk == ''?
                                        <Button block transparent onPress={() => this.uploadImage('kk')}>
                                            <Text style=    {{alignItems:'center', color:'#aaa', fontWeight:'bold'}}> Tambah Foto KK</Text>
                                        </Button>
                                    : 
                                    <View>
                                        <TouchableHighlight onPress={() => this.uploadImage('kk')} style={{marginTop:20}}>
                                            <Image source={{uri:this.state.images_kk.uri}} style={{width:'100%', height:200}}/>
                                        </TouchableHighlight>
                                       { this.state.kirim ?
                                        <Button block success onPress={() => this.kirimPengantar()} style={{marginTop:20, marginBottom:20}}>
                                            <Text style={{alignItems:'center', color:'#FFF', fontWeight:'bold'}}>KIRIM DATA</Text>
                                        </Button>
                                        : null
                                        }
                                    </View>
                                    }
                                </View>

                            } 
                            </Col>
                        </Row>
                        
                    </Grid>
                </Form>
            </Content>
            {this.state.loading ? 
                <View style={styles.notif}>
                    <Process/>
                </View>
            : null
            }
                {this.state.success ? 
                   <View style={styles.notif}>
                        <Done style={{}}>
                            <LottieView
                                ref={animation => {
                                this.animation = animation;
                                }}
                                source={allLogo.success}
                                loop={false}
                                style={styles.lotie}
                            />
                            <Text h2 style={styles.text}>Permhohonan terkirim</Text>
                        </Done>
                    </View>
                : null
                }
        </Container>
        )
    }

    async select_jam() {
        try {
            const {action, hour, minute} = await TimePickerAndroid.open({
              hour: 14,
              minute: 0,
              is24Hour: false, // Will display '2 PM'
            });
            if (action !== TimePickerAndroid.dismissedAction) {
              this.setState({jam : `${hour}:${minute}`});

            }
          } catch ({code, message}) {
            console.warn('Cannot open time picker', message);
          }
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        add_pending : () => dispatch({type : ADD_PENDING})
    }
}

export default connect(null, mapDispatchToProps)(withNavigation(Death));


const styles = StyleSheet.create({
    text_info : {
      fontSize: 14
    },
    inOut : {
        marginHorizontal:10, 
        marginVertical:20,
        marginTop:10, 

    },
    contentInOut : {
        marginTop:10,
        alignItems:'center', 
        justifyContent:'center'
    },
    content_luar_desa : {
        marginHorizontal: 10,
    },
    notif : {
        zIndex:1000001, 
        position:'absolute', 
        left:0, 
        right:0, 
        top:180, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    lotie : {
        width: 150,
        height: 150,
        justifyContent:'center',
        alignItems:'center'
    },
    text : {
        marginTop:-40,
        justifyContent:'center', 
        alignItems:'center', 
        textAlign:'center', 
        paddingBottom:20,
        color:'#000'
    },
})