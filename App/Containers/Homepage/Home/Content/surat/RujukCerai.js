import React,{Component} from 'react';
import {
    Alert,
    StyleSheet,
    StatusBar,
    Image,
    TouchableHighlight
} from 'react-native';
import config from '../../../../../Configs/config';
import { allLogo } from '@assets'
import { new_pengantar_surat, warga_description } from '@Apis';
import ImagePicker from 'react-native-image-picker'
import { withNavigation } from 'react-navigation';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import { 
    Header, 
    Text,
    Title,
    Icon, 
    Content, 
    Button,
    Toast,
    Row,
    Col,
    Item,
    Left, Body, Right, Container, Form,Textarea, Grid, Input, Label, View} from 'native-base';
import {uploadtoServer} from '../../../../../Helper/uploadImage';
import {Process, Done} from '../../../../../components';
import {connect} from 'react-redux';
import { ADD_PENDING } from '@redux_store';



class RujukCerai extends Component {
    constructor(props) {
        super(props);
        this.setDate = this.setDate.bind(this);
        this.state = {
            dataWarga : { nama : '', nik:'', tanggallahir : '', tempatlahir:''},
            jenisSurat : '',
            loading : false,
            success : '',
            images_kk:'',
            images_ktp : '',
            keterangan : "",
            nama_pasangan : "",
            tgl_lahir_pasangan: null,
            nama_ayah : '',
            warganegara : '',
            agama_pasangan : '',
            pekerjaan : '',
            alamat : '',
            perihal : '',
            kirim :true,


        };
    }
    setDate(date) {
        let tanggallahir = moment(date).format('YYYY-MM-DD');
        this.setState({tgl_lahir_pasangan : tanggallahir});
    }

     async componentDidMount() {
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#64e291");
        const { navigation } = this.props;
        const id_warga      =  await navigation.getParam('id_warga');
        const id_surat      =  await navigation.getParam('id_surat');

        warga_description(id_warga).then(async (res) => {
            this.setState({
                dataWarga  : res.data.data,
                jenisSurat : id_surat,
                keterangan : 'ingin membuat surat keterangan rujuk atau cerai',
            });
        });
        
    }
  
    uploadImage(type) {
        const options = {
            quality: 1.0,
            storageOptions: {
              skipBackup: true
            }
        };


        Alert.alert(
            'Tambah Kelengkapan',
            'Silahkan pilih sumber gambar',
            [
                {text: 'Camera', onPress: () => {
                    ImagePicker.launchCamera(options, (res) => {
                        let source = {
                            uri : res.uri,
                            fileName : res.fileName,
                            fileSize : res.fileSize,
                            type : res.type
                        };
                        if (type == 'kk') this.setState({images_kk : source}) 
                        else this.setState({images_ktp : source})
                        
                    });
                }},
                {text: 'Gallery',onPress: () => {
                    ImagePicker.launchImageLibrary(options, (res) => {
                        let source = {
                            uri : res.uri,
                            fileName : res.fileName,
                            fileSize : res.fileSize,
                            type : res.type
                        };

                        if (type == 'kk') this.setState({images_kk : source}) 
                        else this.setState({images_ktp : source})
                    });
                }},
            ],
            {cancelable: true},
        )
       
    } 

    async upload() {
        let form        = await new FormData();
        form.append('foto_kk', { 
            'uri' : this.state.images_kk.uri,
            'name' : this.state.images_kk.fileName,
            'type' : this.state.images_kk.type
        });

        form.append('foto_ktp', { 
            'uri' : this.state.images_ktp.uri,
            'name' : this.state.images_ktp.fileName,
            'type' : this.state.images_ktp.type
        });

        /* send to servers */
        let data = await uploadtoServer(form);
        return data;
      
    }

    async kirimPengantar() {
        this.setState({loading: true, kirim:false});
        let {keterangan} = this.state;
        const data_uri = await this.upload(); 
        const path_url_image = config.url_assets + '/assets/files/dokumen/';
        if(data_uri) {
            if(keterangan) {
                let data = {
                     'pbb' : path_url_image + data_uri.image_pbb,
                     'kk'  : path_url_image + data_uri.image_kk,
                     'keterangan' : keterangan,
                     'perihal' : this.state.perihal,
                     'id_penduduk' :  this.state.dataWarga.id,
                     'id_jenissurat'  : this.state.jenisSurat,
     
                };
                /* send data rt to server */
                    new_pengantar_surat(data).then((res) =>{
                        if(res.data.code == 0)    {
                            this.setState({success: true, loading: false});
                            this.props.add_pending();
                            setTimeout(() => {
                                this.animation.play();
                            },500);
                            setTimeout(() => {
                                this.props.navigation.navigate('Home');
                            },2000);
        
                        } else {
                             this.setState({loading: false, kirim:true});
                            Toast.show({    
                                text: "gagal saat proses kirim data",
                                buttonText: "Oke",
                                type: "danger",
                                overlay : '0.5'
                            });
                        }
                    }); 
                
            } else {
                this.setState({loading: false, kirim:true});
                Toast.show({
                    text: "harap input semua fild",
                    buttonText: "Oke",
                    type: "danger",
                    overlay : '0.5'
                });
            }
        } else {
            this.setState({loading: false, kirim:true});
            Toast.show({
                text: "gagal saat proses kirim data",
                buttonText: "Oke",
                type: "danger",
                overlay : '0.5'
            });
        }
    }

    render() {
        return (
            <Container>
                <Header  style={{backgroundColor:'#64e291'}}>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name='arrow-back' />
                    </Button>
                </Left>
              <Body>
                  <Title>Ket Rujuk/cerai</Title>
              </Body>
              <Right/>
              
            </Header>
            <Content>
                <Form style={{marginHorizontal:20, marginTop:20}}>
                    <Grid>                        
                        <Row style={{marginTop:10}}>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Pemohon</Label>
                                    <Input disabled value={this.state.dataWarga.nama} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <Item stackedLabel> 
                                            <Label>Agama</Label>
                                            <Input disabled value={this.state.dataWarga.agama} style={styles.text_info}/>
                                        </Item>
                                    </Col>
                                    <Row>
                                        <Col>
                                            <Item stackedLabel> 
                                                <Label>Jenis Kelamin</Label>
                                                <Input disabled value={this.state.dataWarga.gender} style={styles.text_info}/>
                                            </Item>
                                        </Col>
                                    </Row>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Nik</Label>
                                    <Input disabled value={this.state.dataWarga.nik} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tanggal Lahir</Label>
                                    <Input disabled value={this.state.dataWarga.tanggallahir} style={styles.text_info}/>
                                </Item>
                            </Col>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tempat Lahir</Label>
                                    <Input underline value={this.state.dataWarga.tempatlahir} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Nama Rt</Label>
                                    <Input underline value={this.state.dataWarga.nama_rt} disabled style={styles.text_info} />
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Rt</Label>
                                    <Input underline disabled value={this.state.dataWarga.rt} style={styles.text_info}/>
                                </Item>
                            </Col>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Rw</Label>
                                    <Input underline disabled value={this.state.dataWarga.rw} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                         <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Perihal</Label>
                                    <Textarea autoCorrect={false}  style={styles.text_info} rowSpan={3} style={{width:'100%'}} value={this.state.perihal} onChangeText={(text) => this.setState({perihal: text})} ></Textarea>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Keterangan</Label>
                                    <Textarea autoCorrect={false}  style={styles.text_info} rowSpan={3} style={{width:'100%'}} value={this.state.keterangan} onChangeText={(text) => this.setState({keterangan: text})} ></Textarea>
                                </Item>
                            </Col>
                        </Row>
                        <Row style={{marginBottom:20}}>
                            <Col >
                            {this.state.images_ktp == ''? 
                                <Button block transparent onPress={() => this.uploadImage('ktp')}>
                                    <Text style=    {{alignItems:'center', color:'#aaa', fontWeight:'bold'}}> Tambah Foto KTP</Text>
                                </Button>
                            : 
                                <View>
                                    <TouchableHighlight onPress={() => this.uploadImage('ktp')} style={{marginTop:20}}>
                                        <Image source={{uri:this.state.images_ktp.uri}} style={{width:'100%', height:200}}/>
                                    </TouchableHighlight>
                                    { this.state.images_kk == ''?
                                        <Button block transparent onPress={() => this.uploadImage('kk')}>
                                            <Text style=    {{alignItems:'center', color:'#aaa', fontWeight:'bold'}}> Tambah Foto KK</Text>
                                        </Button>
                                    : 
                                    <View>
                                        <TouchableHighlight onPress={() => this.uploadImage('kk')} style={{marginTop:20}}>
                                            <Image source={{uri:this.state.images_kk.uri}} style={{width:'100%', height:200}}/>
                                        </TouchableHighlight>
                                       {
                                           this.state.kirim ? 
                                            <Button block success onPress={() => this.kirimPengantar()} style={{marginTop:20, marginBottom:20}}>
                                                <Text style={{alignItems:'center', color:'#FFF', fontWeight:'bold'}}>KIRIM DATA</Text>
                                            </Button>
                                            : null

                                       }
                                    </View>
                                    }
                                </View>

                            } 
                            </Col>
                        </Row>
                        
                    </Grid>
                </Form>
            </Content>
            {this.state.loading ? 
                <View style={styles.notif}>
                    <Process/>
                </View>
            : null
            }
                {this.state.success ? 
                   <View style={styles.notif}>
                        <Done style={{}}>
                            <LottieView
                                ref={animation => {
                                this.animation = animation;
                                }}
                                source={allLogo.success}
                                loop={false}
                                style={styles.lotie}
                            />
                            <Text h2 style={styles.text}>Permhohonan terkirim</Text>
                        </Done>
                    </View>
                : null
                }
        </Container>
        )
    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        add_pending : () => dispatch({type : ADD_PENDING})
    }
}

export default connect(null, mapDispatchToProps)(withNavigation(RujukCerai));


const styles = StyleSheet.create({
    text_info : {
      fontSize: 14
    },
    inOut : {
        marginHorizontal:10, 
        marginVertical:20,
        marginTop:10, 

    },
    contentInOut : {
        marginTop:10,
        alignItems:'center', 
        justifyContent:'center'
    },
    content_luar_desa : {
        marginHorizontal: 10,
    },
    notif : {
        zIndex:1000001, 
        position:'absolute', 
        left:0, 
        right:0, 
        top:180, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    done : {
        zIndex:1000001, 
        position:'absolute',

        justifyContent: 'center', 
        alignItems: 'center'
    },
    lotie : {
        width: 150,
        height: 150,
        justifyContent:'center',
        alignItems:'center'
    },
    text : {
        marginTop:-40,
        justifyContent:'center', 
        alignItems:'center', 
        textAlign:'center', 
        paddingBottom:20,
        color:'#000'
    },
})