import React, { 
  Component 
} from 'react'
import { 
  Image, 
  StyleSheet, 
  StatusBar,
  View,
  TouchableNativeFeedback,
  TouchableOpacity
} from 'react-native'
import { allLogo } from '@assets' 
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Col,  Toast,  Row, Grid, Container, Content, Text, Button, Accordion} from 'native-base';
import { withNavigation } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Shimmer from 'react-native-shimmer-placeholder';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getDataFromServerToClient} from '@redux_store';



class Browse extends Component {
  constructor(props) {
      super(props); 
      this.state = {
        isRefreshing : false,

    }
  }
  async componentDidMount() {
    StatusBar.setBarStyle( 'light-content',true);
    StatusBar.setBackgroundColor("#64e291");
    const { dispatch, nama } = this.props;
    if( !nama ) {
        dispatch(getDataFromServerToClient());
    }
  }
  callbackPress(data) {
    this.props.dataClickFromCild(data);
  }

  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Content style={{backgroundColor:'#eee'}} >
        <LinearGradient colors={['#64e291',  '#00BF4A']} style={{height:hp('35%')}} >
            <Grid style={{flex:1}}>
              <Row>
                <View style={{marginTop:20, marginHorizontal:20, justifyContent:'flex-start'}}>
                    <Text style={{color:'#fff', fontFamily:'Chilanka'}}>Hi {this.props.nama}</Text>
                    <Text style={{fontSize:24, color:'#fff', fontFamily:'Chilanka'}}>{this.props.grettiings} {this.props.nama}</Text>
                </View>
              </Row>
              <Row style={{marginHorizontal:20, justifyContent:'flex-start', marginTop:18,paddingBottom:20}}>
                <TouchableOpacity onPress={ () => this.callbackPress(0)}>
                  <Col style={{alignItems:'center', justifyContent:'flex-start',paddingBottom:3}}>
                    <Text style={{fontSize:24, color:'#fff'}}>{this.props.surat_pending}</Text>
                    <Text style={styles.text_info}> PENDING</Text>
                  </Col>
                </TouchableOpacity>
                <Col style={{alignItems:'center', justifyContent:'flex-start'}}>
                  <Text style={styles.text_info}>|</Text>
                </Col>
                <TouchableOpacity  onPress={ () => this.callbackPress(1)}>
                  <Col style={{alignItems:'center', justifyContent:'flex-start', paddingBottom:3}}>
                    <Text style={{fontSize:24, color:'#fff'}}>{this.props.surat_rejected}</Text>
                    <Text style={styles.text_info}> DITOLAK</Text>
                  </Col>
                </TouchableOpacity>
                <Col style={{alignItems:'center', justifyContent:'flex-start'}}>
                  <Text style={styles.text_info}>|</Text>
                </Col>
                <TouchableOpacity  onPress={ () => this.callbackPress(2)}>
                  <Col style={{alignItems:'center', justifyContent:'flex-start', paddingBottom:3}}>
                    <Text style={{fontSize:24, color:'#fff'}}>{this.props.surat_terbuat}</Text>
                    <Text style={styles.text_info}> SELESAI</Text>
                  </Col>
                </TouchableOpacity>
              </Row>
            </Grid>
          </LinearGradient>
          <ScrollView>
          <Grid style={{flex:1, paddingTop:10, marginHorizontal:6}}>
              <Row>
                <Col style={{alignItems:'center'}}>
                    <TouchableNativeFeedback transparent  onPress={() => navigation.navigate('SearchNik', {'id_surat' : 1})}>
                      <View style={styles.buttonStyle}>
                        <Image source={allLogo.paper} style={{width:40, height:40}}/>
                        <Text uppercase={false} style={styles.textCenter}>Surat Pengantar Umum</Text>
                      </View>
                    </TouchableNativeFeedback>
                </Col>  
                
                <Col style={{alignItems:'center'}}>
                    <TouchableNativeFeedback transparent  onPress={() => navigation.navigate('SearchNik', {'id_surat' : 24})}>
                      <View style={styles.buttonStyle}>
                        <Image source={allLogo.mati} style={{width:40, height:40}}/>
                        <Text uppercase={false} style={styles.textCenter}>Surat Keterangan Kematian</Text>
                      </View>
                  </TouchableNativeFeedback>
                </Col>
               
                <Col style={{alignItems:'center'}}>
                  <TouchableNativeFeedback transparent  onPress={() => navigation.navigate('SearchNik', {'id_surat' : 38})}>
                    <View style={styles.buttonStyle}>
                      <Image source={allLogo.cerai_rujuk} style={{width:40, height:40}}/>
                      <Text uppercase={false} style={styles.textCenter}>Surat Keterangan Rujuk/Cerai</Text>
                    </View>
                  </TouchableNativeFeedback>
                </Col>
              </Row>
              <Row >
              <Col style={{alignItems:'center'}}>
                    <TouchableNativeFeedback onPress={ () => navigation.navigate('SearchNik', {id_surat:12})}>
                      <View style={styles.buttonStyle}>
                          <Image source={allLogo.tidakmampu} style={{width:40, height:40}}/>
                          <Text uppercase={false} style={styles.textCenter}>Surat Ket Kurang Mampu </Text>
                      </View>
                    </TouchableNativeFeedback>
                </Col>
              <Col style={{alignItems:'center', marginHorizontal:0}}>
                    <TouchableNativeFeedback onPress={() => navigation.navigate('SearchNik', {'id_surat' : 13})}>
                      <View style={styles.buttonStyle}>
                        <Image source={allLogo.divorce} style={{width:40, height:40}}/>
                        <Text uppercase={false} style={styles.textCenter}>Surat Keterangan Pernyataan</Text>
                      </View>
                    </TouchableNativeFeedback>
                </Col>
                <Col style={{alignItems:'center'}}>
                    <TouchableNativeFeedback onPress={() => this.comingSoon()}>
                      <View style={[styles.buttonStyle,{backgroundColor:'#ddd'}]}>
                        <Image source={allLogo.addWarga} style={{width:40, height:40}}/>
                        <Text uppercase={false} style={styles.textCenter}>Tambah Warga</Text>
                      </View>
                    </TouchableNativeFeedback>
                </Col>
              </Row>
              <Row style={{ paddingTop:10}}>
                <Col>
                  <View >
                        <Content padder style={{minHeight:300, marginHorizontal: 0}}>
                              <Shimmer autoRun={true} visible={this.props.loading}>
                                <Text h3 style={{color:'#aaa'}}>Pemberitahuan Terbaru</Text>
                              </Shimmer>
                        
                              <Shimmer autoRun={true} visible={this.props.loading} width={wp('100%')} height={hp('15%')} style={{ marginHorizontal:0}}>
                                  <Accordion iconStyle={{ color: "green" }} dataArray={this.props.pemberitahuan} contentStyle={{backgroundColor:'#fff', textAlign:'justify', fontSize:16, fontFamily:'Serif', color:'#000', marginHorizontal:0}} style={{paddingTop:10, marginHorizontal:0}} headerStyle={{fontWeight:'100', fontSize:16}}/>
                              </Shimmer>
                        </Content>
                  </View>
                </Col>
              </Row>
            </Grid>
          </ScrollView>
        </Content>
      </Container>
    );
  }
  comingSoon() {
      Toast.show({
        text: 'Segara hadir', 
        buttonText:'oke',
        duration:2000,
      })
  }
}

const mapStateToProps = (state) => {
  return {
    loading : state.loading, 
    error : state.error,
    loading : true,
    error : '',
    surat_pending : state.surat_pending, 
    surat_rejected: state.surat_rejected,
    surat_terbuat : state.surat_terbuat,
    grettiings    : state.grettiings,
    nama          : state.nama,
    pemberitahuan : state.pemberitahuan,
  }
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchdata : (val) => dispatch({type : 'REFRESS_TO_API', data : val}),
    dispatch
  })
}


export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Browse));



const styles = StyleSheet.create({
  text_info : {
    justifyContent :'flex-start',
    marginTop:10, 
    fontSize:12, 
    fontWeight:'100',
    fontFamily : 'Chilanka',
    color: '#fff'
  },
  textCenter : {
    marginTop:5, 
    fontSize:14, 
    fontFamily : 'Chilanka',
    color:'#aaa', 
    fontWeight: '300',
    alignItems:'center', 
    justifyContent:'center', 
    textAlign:'center',
  },
  buttonStyle : {
    flexDirection:'column',
    marginTop:3,
    marginHorizontal:3,
    backgroundColor:'#fff',
    borderRadius:4,
    width:wp('31.3%'), 
    marginVertical:3,
    height:hp('18.7%'),
    alignItems:'center', 
    justifyContent:'center'
  },
  accordionHeader : {
    fontFamily: 'FiraSans',
    marginHorizontal:0,
  }
})
