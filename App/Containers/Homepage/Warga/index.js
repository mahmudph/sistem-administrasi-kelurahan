import React, { Component } from 'react'
import { 
  Image, 
  StyleSheet, 
  View,
  Alert,
  Dimensions,
  StatusBar,
} from 'react-native'
import {  
  Header, 
  Left, 
  Body, 
  Right , 
  Title, 
  Content, 
  ListItem, 
  Item,
  Icon, 
  Container, 
  Grid,
  Row,
  Col,Thumbnail,
  Input,
  Spinner,
  Label,
  Form, 
  Text, 
  Button
} from 'native-base';
import { withNavigation } from 'react-navigation';
import {get_warga, warga_info_jmlah, warga_description} from '@Apis';
import env from '../../../Configs/config';
import {allLogo} from '@assets';
import SwipeablePanel from 'rn-swipeable-panel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';




class Warga extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      modal :false,
      dataWarga : [],
      search:'',
      jmlKK : null,
      jmlwarga : null,
      swipeablePanelActive : false,
      dess_warga : {},
      spinner : true,
    }
  }
  
  componentDidMount = async () => {
    StatusBar.setBarStyle( 'light-content',true)
    StatusBar.setBackgroundColor("#64e291")

    /* infromasi mengenai seluruh warga */
    warga_info_jmlah().then((res => {
      this.setState({
        jmlwarga : res.data.data_warga,
        jmlKK : res.data.data_kk,
      });
    }));

    /* data warga */
    get_warga().then((res) => {
      if(Array.isArray(res.data.data)) {
          this.setState({
              dataWarga : res.data.data,
              spinner : false,
          });
      } else {
        this.setState({
          dataWarga : [],
          spinner : false,
        });
      }
    }).catch(err=> {
        Alert.alert('Tidak bisa terhubung ke jaringan', err);
    })
  }

  handleupdate = async(val) =>{
    this.setState({search : val});
    this.setState({showLoading : true});
    get_warga({keywords : val}).then((res) => {
      if(Array.isArray(res.data.data)) {
          this.setState({
              dataWarga : res.data.data,
          });
      } else {
        this.setState({
          dataWarga : [],
        });
      }
    }).catch(err=> {
        Alert.alert('Tidak bisa terhubung ke jaringan', err);
    })
  }
  open_description(id) {
    warga_description(id).then((res => {
      this.setState({swipeablePanelActive: true, dess_warga: res.data.data});
    }));
  }
  render() {
    return (
      <Container>
      
            <Content enableOnAndroid>
              <KeyboardAwareScrollView 
              resetScrollToCoords={{ x: 0, y: 0 }}
              scrollEnabled={false}>
                <LinearGradient colors={['#64e291', '#00BF4A']} style={{}}>
                <View flex={1} row style={{minHeight:140 }}> 
                  <Grid style={{minHeight:140 }}>
                    <Row>
                      <Col>
                        <View style={{marginHorizontal:20}}>
                            <Text style={{fontSize:22, color:'#fff', marginTop:20}}>Informasi Mengenai Warga</Text>
                            <Text>Warga Saya</Text>
                        </View>
                      </Col>
                    </Row>
                    <Row style={{marginTop:1, marginBottom:10}}>
                      <Col style={{justifyContent:'center', alignItems:'center'}}>
                        <Image source={allLogo.human} style={{width:130, height:100}}/>
                        <Text style={{fontSize:20, color:'#fff'}}>{this.state.jmlwarga} WARGA | {this.state.jmlKK} KK</Text>
                      </Col>
                    </Row>
                  </Grid>
                </View>
                </LinearGradient>
                <View  style={{backgroundColor:'#fff', marginTop:10, minHeight:300}}>
                    <Item>
                      <Input 
                          placeholder="Cari warga"
                          value = {this.state.search}
                          onChangeText={(val) => this.handleupdate(val)}
                          onClear= {() => this.handleupdate('')}
                          style={{padding:10}}
                          autoCorrect={false}
                          autoCompleteType={false}
                          />
                      <Icon name="search"/>
                    </Item>
                  <View>
                    {  this.state.spinner ? 
                        <View style={{flex:1, alignItems:'center', justifyContent:'center', Height:100, marginTop:60}}>
                          <Spinner/>
                          <Text >Mohon tunggu</Text>
                        </View>
                    :
                    <View>
                    { this.state.dataWarga.length > 0 ?
                      this.state.dataWarga.map((item, key) => {
                        return (
                          <ListItem avatar button={true} onPress={() => this.open_description(item.id)}>
                            <Left>
                                <Thumbnail source={allLogo.kuser} style={{width:30, height:30}}  />
                            </Left>
                            <Body>
                                <Text style={{color:'#000', fontWeight:'small', fontSize:14}}>{item.nama}</Text>
                                <Text note>{item.nik}</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
                        )})
                      : 
                      <View style={{flex:1, alignItems:'center', justifyContent:'center', minHeight:100}}>
                        <View style={{flex:1, alignItems:'center', justifyContent:'center', marginTop:-10}}>
                            <Image source={allLogo.notFound}  style={{width:250, height:200}}/>
                            <Text style={{marginTop:-30}}>data tidak ditemukan</Text>
                        </View>
                      </View>
                    }
                    </View>
                    }
                </View>
              </View>
            </KeyboardAwareScrollView>
            
          </Content>
          <SwipeablePanel  style={{width:50, height:50, flex:1, justifyContent:'flex-end'}}
                  fullWidth
                  isActive={this.state.swipeablePanelActive}
                  onClose={this.closePanel}
                  noBackgroundOpacity={false}
                  onPressCloseButton={this.closePanel}>
                    <View style={{minHeight:100, marginHorizontal:20, marginTop:20}}>
                      <Text>Informasi Warga</Text>
                      <Form style={{marginTop:10}}>
                          <Row>
                              <Col>
                                  <Item stackedLabel> 
                                      <Label>Nama</Label>
                                      <Input disabled value={this.state.dess_warga.nama} style={styles.text_info}/>
                                  </Item>
                              </Col>
                          </Row>
                          <Row>
                              <Col>
                                  <Item stackedLabel> 
                                      <Label>Nik</Label>
                                      <Input disabled value={this.state.dess_warga.nik} style={styles.text_info}/>
                                  </Item>
                              </Col>
                          </Row>
                          <Row>
                              <Col>
                                  <Item stackedLabel> 
                                      <Label>Rt</Label>
                                      <Input disabled value={this.state.dess_warga.rt} style={styles.text_info}/>
                                  </Item>
                              </Col>
                              <Col>
                                  <Item stackedLabel> 
                                      <Label>Rw</Label>
                                      <Input disabled value={this.state.dess_warga.rw} style={styles.text_info}/>
                                  </Item>
                              </Col>
                          </Row>
                          <Row>
                              <Col>
                                  <Item stackedLabel> 
                                      <Label>Tempat Lahir</Label>
                                      <Input disabled value={this.state.dess_warga.tempatlahir} style={styles.text_info}/>
                                  </Item>
                              </Col>
                              <Col>
                                  <Item stackedLabel> 
                                      <Label>Tanggal Lahir</Label>
                                      <Input disabled value={this.state.dess_warga.tanggallahir} style={styles.text_info}/>
                                  </Item>
                              </Col>
                          </Row>
                        </Form>
                    </View>
                </SwipeablePanel>
        </Container>
    )
  }

  closePanel = () => {
    this.setState({swipeablePanelActive : false});
  }

}




export default withNavigation(Warga);

const styles = StyleSheet.create({
  text_info : {
    fontSize: 14
  }
})
