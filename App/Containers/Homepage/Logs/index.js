import React, {Component }  from 'react';
import {StatusBar} from 'react-native';
import { 
    Container,
    Header, 
    Button,
    Icon, 
    Text,
    Left, Body, Right, Title, Tab, Tabs, TabHeading,ActionSheet
} from 'native-base';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Pending, Rejected, Finished } from './TabLogs';




export default class Logs extends Component {
    constructor(props){
        super(props);
        this.state = {
            data : [],
            filter : 'all',
            loading : true,
            activeTab : 0
        }
    }
    componentWillMount() {
        let {active} = this.props;
        if(active != null ) {
            this.setState({activeTab: active});
        } else {
            this.setState({activeTab: 0});
        }
    }

    componentDidMount() {
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#64e291")
    }

    filterTab() {
        var BUTTONS = [
            { text: "Surat Pengantar Umum", icon: "paper", iconColor: "#2c8ef4", size:14, des:'Pengantar' },
            { text: "Surat Ket kematian", icon: "paper", iconColor: "#2c8ef4", des: 'Keterangan Kematian'},
            { text: "Surat Ket Rujuk/Cerai ", icon: "paper", iconColor: "#2c8ef4", des: 'Keterangan Pengantar Rujuk/Cerai' },
            { text: "Surat Ket Tidak Mampu", icon: "paper", iconColor: "#2c8ef4", des: 'Keterangan Kurang Mampu' },
            { text: "Surat Pernyataan", icon: "paper", iconColor: "#2c8ef4", des:'Pernyataan' },
            { text: "Tampilkan semua surat", icon: "paper", iconColor: "#2c8ef4", des:'all' },
            { text: "Cancel", icon: "close", iconColor: "#25de5b", des:'close'}
          ];
        var DESTRUCTIVE_INDEX = 3;
        var CANCEL_INDEX = 5;
        ActionSheet.show(
            {
              options: BUTTONS,
              cancelButtonIndex: CANCEL_INDEX,
              destructiveButtonIndex: DESTRUCTIVE_INDEX,
              title: "Filter data pengajuan surat"
            },
            buttonIndex => {
              this.setState({ filter: BUTTONS[buttonIndex].des, loading:true} );
            }
          )
    }

    render() {
        return (
        <Container>
            
            <Header noShadow hasTabs style={{backgroundColor:'#00BF4A'}}>
                <Left/>
                <Body>
                    <Title>Riwayat Surat  </Title>
                </Body>
                <Right>
                    <Button transparent onPress={() => this.filterTab()}>
                        <AwesomeIcon name="filter" size={16} style={{color:'#fff'}} />
                    </Button>
                </Right>
            </Header>
                <Tabs initialPage={this.state.activeTab} style={{backgroundColor:'64e291'}} tabContainerStyle={{elevation:0}} onChangeTab={() => this.setState({filter: 'all'})}>
                        <Tab heading={
                            <TabHeading style={{backgroundColor: '#00BF4A'}}>
                                <Icon style={{fontSize:18, color:'#fff'}} name="time"/>
                                <Text bold white style={{color:'#fff'}}> Menunggu</Text>
                            </TabHeading>
                        }>
                        <Pending filter={this.state.filter} refres={this.state.loading}/>
                    </Tab>
                    <Tab heading={
                        <TabHeading style={{backgroundColor: '#00BF4A'}} >
                            <Icon style={{color:'#fff', fontSize:18}} name="alert"/>
                            <Text bold white style={{color:'#fff'}}> Ditolak</Text>
                        </TabHeading>
                    }>
                        <Rejected filter={this.state.filter}  refres={this.state.loading}/>
                    </Tab>
                    <Tab heading={
                        <TabHeading style={{backgroundColor: '#00BF4A'}} >
                            <Icon style={{color:'#fff', fontSize:18}} name="alert"/>
                            <Text bold white style={{color:'#fff'}}> Selesai</Text>
                        </TabHeading>
                    }>
                        <Finished filter={this.state.filter}  refres={this.state.loading}/>
                    </Tab>
                
                </Tabs>
        </Container>
        )

    }
}
