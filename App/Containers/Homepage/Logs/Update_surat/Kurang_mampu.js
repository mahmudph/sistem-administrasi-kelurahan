import React,{Component} from 'react';
import {
    Alert,
    StyleSheet,
    StatusBar,
    Image,
    TouchableHighlight
} from 'react-native';
import { allLogo } from '@assets'
import { save_update, edit_surat } from '@Apis';
import {takePictureandSend, getPictureFromLibrary} from '@AuthCheck';
import { withNavigation } from 'react-navigation';
import LottieView from 'lottie-react-native';
import { 
    Header, 
    Title,
    Text,
    Icon, 
    Content, 
    Button,
    Toast,
    Row,
    Col,
    Item,
    Left, Body, Right, Container, Form,Textarea, Grid, Input, Label, View,
} from 'native-base';
import {Process, Done} from '../../../../components';


class TidakMampuUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataWarga : { nama : '', nik:'', tanggallahir : '', tempatlahir:''},
            jenisSurat : '',
            loading : false,
            perihal : '',
            success : '',
            images_kk:'',
            images_ktp : '',
            keterangan : "",
            id_penghubung_pengantar : '',
            id_surat : '',
            kirim : true,

        };
    }
     async componentDidMount() {
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#64e291");
        const {navigation}   = this.props;
        const id_jenissurat  =  await navigation.getParam('id_jenissurat');
        const id_surat       =  await navigation.getParam('id_surat');
        const id_penghubung  =  await navigation.getParam('id_surat_masuk');
        
        edit_surat(id_surat, id_jenissurat).then((res) => {
            if(res.data.data != null) {
                this.setState(
                    {
                        dataWarga : res.data.data, 
                        perihal : res.data.data.keperluan,
                        keterangan : res.data.data.keterangan,
                        images_kk :  res.data.data.kk,
                        images_ktp : res.data.data.pbb,
                        id_penghubung_pengantar : id_penghubung,
                        id_surat : id_surat
                    })
            }

        });
        
    }
  
    uploadImage(type) {
        Alert.alert(
            'Tambah Kelengkapan',
            'Silahkan pilih sumber gambar',
            [
                {text: 'Camera', onPress: () => {
                    takePictureandSend(type, (err, result) => {
                        if(err) {
                            Toast.show({
                                text:err, 
                                buttonText:'oke',
                                type: 'danger',
                                duration:2000,
                            });
                        } 
                        if(type == 'kk') this.setState({images_kk: result});
                        else this.setState({images_ktp: result});
                    });
                }},
                {text: 'Gallery',onPress: () => {
                    getPictureFromLibrary(type, (err, result) => {
                        if(err) {
                            Toast.show({
                                text:err, 
                                buttonText:'oke',
                                type: 'danger',
                                duration:2000,
                            });
                        } 

                        if(type == 'kk') this.setState({images_kk: result});
                        else this.setState({images_ktp: result});

                    })
                }},
            ],
            {cancelable: true},
        )
       
    } 

    async kirimPengantar() {
        this.setState({loading: true, kirim : false});
        let data = {
            'pbb' : this.state.images_ktp,
            'kk'  : this.state.images_kk,
            'perihal'    : this.state.perihal,
            'keterangan' : this.state.keterangan,
            'id_penduduk'    : this.state.dataWarga.id_pend,
            'id_surat_masuk' : this.state.id_surat,
            'id_jenissurat'  : this.state.dataWarga.id_jenissurat,
            'id_penghubung_pengantar' : this.state.id_penghubung_pengantar
        };
        /* send data rt to server */
        try{
            save_update(data).then((res) => {
                if(res.data.code == 0) {
                    this.setState({success: true, loading:false});
                    setTimeout(() => {
                        this.animation.play()
                    },500);
                    setTimeout(() => {
                        this.props.navigation.navigate('Home');
                    },1500);
                } else {
                    this.setState({loading: false, kirim :true});
                    Toast.show({
                        text: "gagal saat proses kirim data",
                        buttonText: "Oke",
                        type: "danger",
                        overlay : '0.5'
                    });
                }
            });   
           
        }catch(err){
            this.setState({loading: false, kirim :true});
            Toast.show({
                text: "gagal saat proses kirim data",
                buttonText: "Oke",
                type: "danger",
                overlay : '0.5'
            });
        }
    }

    render() {
        return (
            <Container>
                <Header  style={{backgroundColor:'#64e291'}}>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name='arrow-back' />
                    </Button>
                </Left>
              <Body>
                  <Title>Pengantar Tidak Mampu</Title>
              </Body>
              <Right/>
              
            </Header>
            <Content>
                <Form style={{marginHorizontal:20, marginTop:20}}>
                    <Grid>                        
                        <Row style={{marginTop:10}}>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Pemohon</Label>
                                    <Input disabled value={this.state.dataWarga.nama} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <Item stackedLabel> 
                                            <Label>Agama</Label>
                                            <Input disabled value={this.state.dataWarga.agama} style={styles.text_info}/>
                                        </Item>
                                    </Col>
                                    <Row>
                                        <Col>
                                            <Item stackedLabel> 
                                                <Label>Jenis Kelamin</Label>
                                                <Input disabled value={this.state.dataWarga.gender} style={styles.text_info}/>
                                            </Item>
                                        </Col>
                                    </Row>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Nik</Label>
                                    <Input disabled value={this.state.dataWarga.nik} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tanggal Lahir</Label>
                                    <Input disabled value={this.state.dataWarga.tanggallahir} style={styles.text_info}/>
                                </Item>
                            </Col>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Tempat Lahir</Label>
                                    <Input underline value={this.state.dataWarga.tempatlahir} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Nama Rt</Label>
                                    <Input underline value={this.state.dataWarga.nama_rt} disabled style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Rt</Label>
                                    <Input underline disabled value={this.state.dataWarga.rt} style={styles.text_info}/>
                                </Item>
                            </Col>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Rw</Label>
                                    <Input underline disabled value={this.state.dataWarga.rw} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Keperluan *</Label>
                                    <Input underline autoCapitalize={false} value={this.state.perihal} autoCorrect={false} onChangeText={(val) => this.setState({'perihal' : val})} style={styles.text_info}/>
                                </Item>
                            </Col>
                        </Row>
                      
                        <Row>
                            <Col>
                                <Item stackedLabel> 
                                    <Label>Keterangan</Label>
                                    <Textarea autoCorrect={false}  style={styles.text_info} rowSpan={3} style={{width:'100%'}} value={this.state.keterangan} onChangeText={(text) => this.setState({keterangan: text})} ></Textarea>
                                </Item>
                            </Col>
                        </Row>
                        <Row style={{marginBottom:20}}>
                            <Col >
                            {this.state.images_ktp == ''? 
                                <Button block transparent onPress={() => this.uploadImage('ktp')}>
                                    <Text style=    {{alignItems:'center', color:'#aaa', fontWeight:'bold'}}> Tambah Foto KTP</Text>
                                </Button>
                            : 
                                <View>
                                    <TouchableHighlight onPress={() => this.uploadImage('ktp')} style={{marginTop:20}}>
                                        <Image source={{uri:this.state.images_ktp.uri ? this.state.images_ktp.uri : this.state.images_ktp}} style={{width:'100%', height:200}}/>
                                    </TouchableHighlight>
                                    { this.state.images_kk == ''?
                                        <Button block transparent onPress={() => this.uploadImage('kk')}>
                                            <Text style=    {{alignItems:'center', color:'#aaa', fontWeight:'bold'}}> Tambah Foto KK</Text>
                                        </Button>
                                    : 
                                    <View>
                                        <TouchableHighlight onPress={() => this.uploadImage('kk')} style={{marginTop:20}}>
                                            <Image source={{uri:this.state.images_kk.uri ? this.state.images_kk.uri : this.state.images_kk}} style={{width:'100%', height:200}}/>
                                        </TouchableHighlight>
                                       {
                                           this.state.kirim ? 
                                            <Button block success onPress={() => this.kirimPengantar()} style={{marginTop:20, marginBottom:20}}>
                                                <Text style={{alignItems:'center', color:'#FFF', fontWeight:'bold'}}>KIRIM DATA</Text>
                                            </Button>
                                            : null
                                       }
                                    </View>
                                    }
                                </View>

                            } 
                            </Col>
                        </Row>
                        
                    </Grid>
                </Form>
            </Content>
            {this.state.loading ? 
                <View style={styles.notif}>
                    <Process/>
                </View>
            : null
            }
                {this.state.success ? 
                   <View style={styles.notif}>
                        <Done style={{}}>
                            <LottieView
                                ref={animation => {
                                this.animation = animation;
                                }}
                                source={allLogo.success}
                                loop={false}
                                style={styles.lotie}
                            />
                            <Text h2 style={styles.text}>Permhohonan terkirim</Text>
                        </Done>
                    </View>
                : null
                } 
        </Container>
        )
    }
}

export default withNavigation(TidakMampuUpdate);
const styles = StyleSheet.create({
    text_info : {
      fontSize: 14
    },

    notif : {
        zIndex:1000001, 
        position:'absolute', 
        left:0, 
        right:0, 
        top:180, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    done : {
        zIndex:1000001, 
        position:'absolute',

        justifyContent: 'center', 
        alignItems: 'center'
    },
    lotie : {
        width: 150,
        height: 150,
        justifyContent:'center',
        alignItems:'center'
    },
    text : {
        marginTop:-40,
        justifyContent:'center', 
        alignItems:'center', 
        textAlign:'center', 
        paddingBottom:20,
        color:'#000'
    },
})
