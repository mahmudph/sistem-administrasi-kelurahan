import React, {
    Component
} from 'react';
import { 
    Container,
    Content,
    Text,
    Spinner,
    Row, 
    Col,
    Button,
    List, ListItem, Left, Body, Right, Thumbnail, Toast
} from 'native-base';
import {withNavigation} from 'react-navigation';
import {
    View,
    Image,
    StatusBar
} from 'react-native';
import {surat_rejected, hapus_surat} from '@Apis';
import SwipeablePanel from 'rn-swipeable-panel';
import {choseActionUpdate} from '@AuthCheck';
import moment from 'moment';
import 'moment/locale/id';
import {allLogo} from '@assets';
import {connect} from 'react-redux';
import {DEL_PENDING} from '@redux_store';


class Rejected extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data_rejected : [],
            spinner : true,
            id_surat_active :'', 
            id_jenissurat : '', 
            id_surat_masuk_penghubung : '',
            swipeablePanelActive : false,
            currentFilter : 'all'
        }
    }

    componentDidMount() {
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#64e291")
        surat_rejected().then((res => {
            this.setState({
                data_rejected : res.data.data, 
                data_rejected_backup : res.data.data,
                spinner : false
            });
        }));
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.filter == 'close' ||  (nextProps.filter == this.state.currentFilter)) {
            this.setState({spinner : !this.props.refres});
        } else {
            this.setState({spinner : this.props.refres});
        }
    }
    componentDidUpdate() {
        if((this.props.filter != this.state.currentFilter) && this.props.filter != 'close') {
            if(this.props.filter == 'all') {
                setTimeout(() => {
                    this.setState({data_rejected: this.state.data_rejected_backup, currentFilter: this.props.filter, spinner:false});
                }, 1000);
            } else {
                setTimeout(() => {
                    let data = this.state.data_rejected_backup.filter((item,i) => {
                        if(item.surat == this.props.filter) {
                            return item;
                        }       
                    });
                    this.setState({data_rejected: data, currentFilter: this.props.filter, spinner:false});
                }, 1000);
            }
        }
        console.log(this.props.filter , this.state.currentFilter);
    }
    open_swep(id, id_surat, id_penghubung) {
        this.setState({
            swipeablePanelActive: true, 
            id_surat_active : id, 
            id_jenissurat : id_surat,
            id_surat_masuk_penghubung : id_penghubung
        });
    }
    closePanel = () => {
        this.setState({swipeablePanelActive : false});
    }

    render() {
        moment.locale('id');
        return(
            <Container>
                <Content>
                    <List>
                        { this.state.spinner ?
                        <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:200}}>
                            <Spinner/>
                            <Text h2 style={{color:'#aaa'}}>Mohon tunggu...</Text>
                        </View> 
                        : 
                        <View>
                                { this.state.data_rejected.length > 0 ?
                                    this.state.data_rejected.map((item, i) => {
                                    return(
                                        <ListItem key={i} avatar button={true} onPress={() => this.open_swep(item.id_suratmasuk, item.id_jenissurat, item.id_surat_masuk_penampung)}>
                                            <Left>
                                                <Thumbnail  source={allLogo.kuser} style={{width:30, height:30}}  />
                                            </Left>
                                            <Body>
                                                <Text>{item.pemohon}</Text>
                                                <Text numberOfLines={1} note>{item.surat}</Text>
                                            </Body>
                                            <Right>
                                                <Text note>{moment(item.tgl_masuk).startOf('hour').fromNow() }</Text>
                                            </Right>
                                        </ListItem>
                                    )
                                })
                            : 
                                <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:50}}>
                                     <Image source={allLogo.notFound}  style={{width:250, height:200, alignItems:'center', justifyContent:'center'}}/>
                                    <Text style={{marginTop:-30}}>data tidak ditemukan</Text>
                                </View>
                            }
                            </View>
                            
                        }
                    </List>
                </Content>
                <SwipeablePanel  style={{width:150, height:300, flex:1, justifyContent:'flex-end'}}
                    fullWidth
                    isActive={this.state.swipeablePanelActive}
                    onClose={this.closePanel}
                    noBackgroundOpacity={false}
                    onPressCloseButton={this.closePanel}>
                    <View style={{marginHorizontal:20, marginTop:20}}>
                        <Text>Lanjuti Surat</Text>
                        <Row style={{marginTop:10}}>
                            <Col> 
                                <Button success block onPress={() => this.edit_surat()}>
                                    <Text>Edit</Text>
                                </Button>
                            </Col>
                            <Col> 
                                <Button danger block onPress={() => this.hapus_surat()}>
                                    <Text>hapus</Text>
                                </Button>
                            </Col>
                        </Row>
                    </View>
                </SwipeablePanel>
            </Container>
                
        );
    }
    hapus_surat() {
        const {id_surat_active, id_jenissurat, id_surat_masuk_penghubung} = this.state;
        this.props.min_pendding();
        let data = {
            id_jenissurat : id_jenissurat,
            id_surat_masuk: id_surat_active,
            id_surat_masuk_penghubung : id_surat_masuk_penghubung
        }

        hapus_surat(data).then((res) => {
            if(res.data.code == 1) {
                Toast.show({
                    text : res.data.msg,
                    type : 'success',
                    buttonText : 'ok',
                    duration: 3000,
                })
                let data = this.state.data_rejected.filter(word => word.id_suratmasuk != id_surat_active);
                this.setState({ data_rejected : data,swipeablePanelActive : false});
            } else {
                Toast.show({
                    text : 'gagal menghapus surat',
                    type : 'success',
                    buttonText : 'ok',
                    duration: 3000,
                })
            }
        });
    }

    edit_surat() {
        try {
            const { id_surat_active, id_jenissurat, id_surat_masuk_penghubung } = this.state;
            var link_action =  choseActionUpdate(id_jenissurat);
            var data = {id_surat : id_surat_active, id_jenissurat: id_jenissurat, id_surat_masuk : id_surat_masuk_penghubung };
            this.props.navigation.navigate(link_action, data);
        } catch(e) {
            Toast.show({
                type:'warning',
                text:'kealahan terjadi tanpa terdudaga',
                buttonText : 'ok',
                duration: 3000,
            })
        }
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        min_pendding : () => dispatch({type : DEL_PENDING})
    }
}
export default connect(null, mapDispatchToProps)(withNavigation(Rejected))