import React, {
    Component
} from 'react';
import { 
    Container,
    Button,
    Row,
    Col,
    Text,
    Spinner,
    List, ListItem, Left, Body, Right, Thumbnail,Content, Toast
} from 'native-base';
import {withNavigation} from 'react-navigation';
import SwipeablePanel from 'rn-swipeable-panel';
import {
    View,
    Image,
    StatusBar
} from 'react-native';
import {get_surat_finished} from '@Apis';
import moment from 'moment';
import 'moment/locale/id';
import {allLogo} from '@assets';
import {choseActionUpdate} from '@AuthCheck';
import {connect} from 'react-redux';
import {DEL_PENDING} from '@redux_store';



class Finished extends Component {
    constructor(props) {
        super(props);
        this.state = {
            swipeablePanelActive : false,
            data_terbuat : [],
            data_terbuat_backup : [],
            spinner : true,
            id_surat_active : null,
            id_jenissurat : null,
            id_surat_masuk_penampung : null,
            currentFilter : 'all',

        }
    }
    componentDidMount() {
        alert('sdfsdfhsdff');
        StatusBar.setBarStyle( 'light-content',true);
        StatusBar.setBackgroundColor("#64e291");
        
        get_surat_finished().then((res) => {  
            this.setState({
                data_terbuat: res.data.data,  spinner : false,
                data_terbuat_backup : res.data.data,
            });
            console.log(res);
            alert(res.data.data.pemohon);
        });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.filter == 'close' || (nextProps.filter == this.state.currentFilter)) {
            this.setState({spinner : !this.props.refres});
        } else {
            this.setState({spinner : true});
        }
    }
    componentDidUpdate() {
        if((this.props.filter != this.state.currentFilter) && this.props.filter != 'close' ) {
            if(this.props.filter == 'all') {
                setTimeout(() => {
                    this.setState({data_terbuat: this.state.data_terbuat_backup, currentFilter: this.props.filter, spinner:false});
                }, 1000);

            } else {
                setTimeout(() => {
                    let data = this.state.data_terbuat_backup.filter((item,i) => {
                        if(item.surat == this.props.filter) {
                            return item;
                        }       
                    });
                    this.setState({data_terbuat:data, currentFilter: this.props.filter, spinner:false});
                }, 1000);
            }
        }
    }
    open_swep(id, id_surat, id_penghubung) {
        this.setState({
            swipeablePanelActive: true, 
            id_surat_active : id, 
            id_jenissurat : id_surat,
            id_surat_masuk_penampung : id_penghubung
        });
    }
    closePanel = () => {
        this.setState({swipeablePanelActive : false});
    }

    render() {
        moment.locale('id');
        return(
            <Container>
                <Content>
                    <List>
                        { this.state.spinner ?
                        <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:200}}>
                            <Spinner/>
                            <Text h2 style={{color:'#aaa'}}>Mohon tunggu... </Text>
                        </View> 
                        : 
                        <View>
                            { this.state.data_terbuat.length > 0 ?
                                this.state.data_terbuat.map((item, i) => {
                                    return(
                                        <ListItem key={i} avatar button={true} onPress={() => this.open_swep(item.id_suratmasuk, item.id_jenissurat, item.id_surat_masuk_penampung)}>
                                            <Left>
                                                <Thumbnail  source={allLogo.kuser} style={{width:30, height:30}}  />
                                            </Left>
                                            <Body>
                                                <Text style={{fontSize:14}}>{item.pemohon}</Text>
                                                <Text numberOfLines={1} note>{item.surat}</Text>
                                            </Body>
                                            <Right>
                                                <Text note>{moment(item.tgl_masuk).startOf('hour').fromNow() }</Text>
                                            </Right>
                                        </ListItem>
                                    )
                                })
                            : 
                                <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:150}}>
                                    <Image source={allLogo.notFound}  style={{width:250, height:200, alignItems:'center', justifyContent:'center'}}/>
                                    <Text style={{marginTop:-30}}>data tidak ditemukan</Text>
                                </View>
                            }
                        </View>
                        }
                    </List>
                
                </Content>
            </Container>
        );
    }
  
}

const mapDispatchToProps = (dispatch) => {
    return {
        min_pendding : () => dispatch({type : DEL_PENDING})
    }
}
export default connect(null, mapDispatchToProps)(withNavigation(Finished));