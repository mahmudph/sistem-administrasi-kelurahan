import React, {
    Component
} from 'react';
import { 
    Container,
    Button,
    Row,
    Col,
    Text,
    Spinner,
    List, ListItem, Left, Body, Right, Thumbnail,Content, Toast
} from 'native-base';
import {withNavigation} from 'react-navigation';
import SwipeablePanel from 'rn-swipeable-panel';
import {
    View,
    Image,
    StatusBar
} from 'react-native';
import {ShowLogs, hapus_surat} from '@Apis';
import moment from 'moment';
import 'moment/locale/id';
import {allLogo} from '@assets';
import {choseActionUpdate} from '@AuthCheck';
import {connect} from 'react-redux';
import {DEL_PENDING} from '@redux_store';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';



class TerbuatLog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            swipeablePanelActive : false,
            data_terbuat : [],
            data_terbuat_backup : [],
            spinner : true,
            id_surat_active : null,
            id_jenissurat : null,
            id_surat_masuk_penampung : null,
            currentFilter : 'all',

        }
    }
    componentWillMount() {
        StatusBar.setBarStyle( 'light-content',true)
        StatusBar.setBackgroundColor("#64e291")
        ShowLogs().then((res) => {  
            this.setState({
                data_terbuat: res.data.data,  spinner : false,
                data_terbuat_backup : res.data.data,
            });
        });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.filter == 'close' || (nextProps.filter == this.state.currentFilter)) {
            this.setState({spinner : !this.props.refres});
        } else {
            this.setState({spinner : true});
        }
    }
    componentDidUpdate() {
        if((this.props.filter != this.state.currentFilter) && this.props.filter != 'close' ) {
            if(this.props.filter == 'all') {
                setTimeout(() => {
                    this.setState({data_terbuat: this.state.data_terbuat_backup, currentFilter: this.props.filter, spinner:false});
                }, 1000);

            } else {
                setTimeout(() => {
                    let data = this.state.data_terbuat_backup.filter((item,i) => {
                        if(item.surat == this.props.filter) {
                            return item;
                        }       
                    });
                    this.setState({data_terbuat:data, currentFilter: this.props.filter, spinner:false});
                }, 1000);
            }
        }
    }
    open_swep(id, id_surat, id_penghubung) {
        this.setState({
            swipeablePanelActive: true, 
            id_surat_active : id, 
            id_jenissurat : id_surat,
            id_surat_masuk_penampung : id_penghubung
        });
    }
    closePanel = () => {
        this.setState({swipeablePanelActive : false});
    }

    render() {
        moment.locale('id');
        return(
            <Container>
                <Content>
                    <List>
                        { this.state.spinner ?
                        <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:200}}>
                            <Spinner/>
                            <Text h2 style={{color:'#aaa'}}>Mohon tunggu... </Text>
                        </View> 
                        : 
                        <View>
                            { this.state.data_terbuat.length > 0 ?
                                this.state.data_terbuat.map((item, i) => {
                                    return(
                                        <ListItem key={i} avatar button={true} onPress={() => this.open_swep(item.id_suratmasuk, item.id_jenissurat, item.id_surat_masuk_penampung)}>
                                            <Left>
                                                <Thumbnail  source={allLogo.kuser} style={{width:30, height:30}}  />
                                            </Left>
                                            <Body>
                                                <Text style={{fontSize:14}}>{item.pemohon}</Text>
                                                <Text numberOfLines={1} note>{item.surat}</Text>
                                            </Body>
                                            <Right>
                                                <Text note>{moment(item.tgl_masuk).startOf('hour').fromNow() }</Text>
                                            </Right>
                                        </ListItem>
                                    )
                                })
                            : 
                                <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:150}}>
                                    <Image source={allLogo.notFound}  style={{width:250, height:200, alignItems:'center', justifyContent:'center'}}/>
                                    <Text style={{marginTop:-30}}>data tidak ditemukan</Text>
                                </View>
                            }
                        </View>
                        }
                    </List>
                
                </Content>
                <SwipeablePanel  style={{width:150, height:300, flex:1, justifyContent:'flex-end'}}
                fullWidth
                isActive={this.state.swipeablePanelActive}
                onClose={this.closePanel}
                noBackgroundOpacity={false}
                onPressCloseButton={this.closePanel}>
                <View style={{marginHorizontal:20, marginTop:20}}>
                    <Text>Lanjuti Surat</Text>
                    <Row style={{marginTop:10}}>
                        <Col> 
                            <Button success block onPress={() => this.edit_surat()}>
                                <Text>Edit</Text>
                            </Button>
                        </Col>
                        <Col> 
                            <Button danger block onPress={() => this.hapus_surat()}>
                                <Text>batalkan</Text>
                            </Button>
                        </Col>
                    </Row>
                </View>
                </SwipeablePanel>
            </Container>
        );
    }
    hapus_surat() {
        
        const {id_surat_active, id_jenissurat, id_surat_masuk_penampung} = this.state;
        let data = {
            id_jenissurat : id_jenissurat,
            id_surat_masuk: id_surat_active,
            id_surat_masuk_penghubung : id_surat_masuk_penampung
        }
        this.props.min_pendding();
        hapus_surat(data).then((res) => {
            if(res.data.code == 1) {
                Toast.show({
                    text : res.data.msg,
                    type : 'success',
                    buttonText : 'ok',
                    duration: 3000,
                })
                let data = this.state.data_terbuat.filter(word => word.id_suratmasuk != id_surat_active);
                this.setState({ data_terbuat : data,swipeablePanelActive : false});
            } else {
                Toast.show({
                    text : 'gagal menghapus surat',
                    type : 'success',
                    buttonText : 'ok',
                    duration: 3000,
                })
            }
        });
    }

    async edit_surat() {
        try {
            const { id_surat_active, id_jenissurat, id_surat_masuk_penampung } = this.state;
            var link_action = await choseActionUpdate(id_jenissurat);


            var data = {id_surat : id_surat_active, id_jenissurat: id_jenissurat, id_surat_masuk : id_surat_masuk_penampung };
            this.props.navigation.navigate(link_action, data);
        } catch(e) {
            Toast.show({
                type:'warning',
                text:'kealahan terjadi tanpa terdudaga',
                buttonText : 'ok',
                duration: 3000,
            })
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        min_pendding : () => dispatch({type : DEL_PENDING})
    }
}
export default connect(null, mapDispatchToProps)(withNavigation(TerbuatLog));