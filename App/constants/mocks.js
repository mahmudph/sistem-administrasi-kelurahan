
/* sosial media linking  */
const settingList = [
  {
    id: 1,
    name: 'Facebook',
    avatar_url: require('../assets/icons/facebook.png'),
    subtitle: '',
    urlsite : 'http://www.facebbok.com/bdrpo_programer/'
  },
  {
    id: 2,
    name: 'Instagram',
    avatar_url: require('../assets/icons/instagram.png'),
    subtitle: '',
    urlsite : 'https://www.instagram.com/Binadarma_programmer'
  },
  // {
  //   id: 3,
  //   name: 'Binadarma Programer',
  //   avatar_url: require('../assets/icons/bdpro.jpg'),
  //   subtitle: '',
  //   urlsite : 'http://bdpro.binadarma.ac.id'
  // }
]

const infoApp = 
{ 
    version : "Beta 1.0.2",
    created : "Bdpro",
    url_ratting : "https://play.google.com/store/apps/details?id=com.whatsapp&hl=in"
}



/* applicaiton landing intro  */

const slides = [
  {
    key: 's1',
    text: 'Semuanya Berada pada Genggaman Anda Serta Mudah digunakan ',
    title: 'Mudah digunakan',
    image:  require('../assets/icons/introApp/smartphone.png'),
    backgroundColor: '#52de97',
  },
  {
    key: 's2',
    title: 'Buat Surat Pengantar',
    text: 'Cukup klik, klik dan pihak kelurahan akan menerima permohonan anda',
    image: require('../assets/icons/introApp/paper-plane.png'),
    backgroundColor: '#febe29',
  },
  {
    key: 's3',
    title: 'Respon yang Cepat',
    text: 'Warga Pemohon Dapat Langsung Datang ke Kelurahan untuk Mengambil Surat',
    image: require('../assets/icons/introApp/time.png'),
    backgroundColor: '#22bcb5',
  },
];





/*  end aplication landing intro  */

export {
  settingList,
  slides,
  infoApp
}