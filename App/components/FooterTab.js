import {Footer, Button,  FooterTab, Icon, Text} from 'native-base';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import React, {Component} from 'react';
import { StyleSheet, Image } from 'react-native';
import {allLogo} from '@assets';
export default class FooterTabHome extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return(
                <Footer style={{position:'absolute', bottom:0}} footerHeight={40} >
                    <FooterTab style={{backgroundColor:'#E8F5C8'}}>
                        <Button vertical onPress={this.props.homPress}>
                            {/* <Image source={allLogo.home_md} style={style.icon_footer_tab} /> */}
                            <FontAwesome5 name={'home'} style={[this.props.homActive  ?  style.active_color_tab : style.color_tab, {fontSize:20}]} solid />
                            <Text active style={this.props.homActive  ?  style.active_color_tab : style.color_tab}>HOME</Text>
                        </Button>
                        <Button bridge vertical onPress={this.props.historyPress}>
                            {/* <Image source={allLogo.notif_md} style={{width:35, height:20}} /> */}
                            <FontAwesome5 name={'history'} style={[this.props.hisActive  ?  style.active_color_tab : style.color_tab, {fontSize:20}]} solid />
                            <Text style={this.props.hisActive  ?  style.active_color_tab : style.color_tab}>RIWAYAT</Text>
                        </Button>
                        <Button vertical  onPress={this.props.wargaPrss}>
                            {/* <Image source={allLogo.people_md} style={{width:30, height:20}} /> */}
                            <FontAwesome5 name={'users'} style={[this.props.wargaActive  ?  style.active_color_tab : style.color_tab, {fontSize:20}]} solid />
                            <Text style={this.props.wargaActive  ?  style.active_color_tab : style.color_tab}>WARGA</Text>
                        </Button>
                        <Button vertical onPress={this.props.logsPress} >
                            <Icon name="settings" style={[this.props.settingActive  ?  style.active_color_tab : style.color_tab, {fontSize:20}]} />
                            {/* <FontAwesome5 name={'user'} style={{fontSize:20, color:'#aaa' }} solid /> */}
                            {/* <Image  source={allLogo.setting_md} style={{width:30, height:20}} /> */}
                            <Text style={this.props.settingActive  ?  style.active_color_tab : style.color_tab}>SETTINGS</Text>
                            
                        </Button>
                    </FooterTab>
                </Footer>
      );
    }

}

const style = StyleSheet.create({
    icon_footer_tab : {
        width: 20, 
        height: 20
    },
    active_color_tab : {
        color : '#00BF4A',
    },
    color_tab : {
        color: '#000'
    }
})