import React,{Component} from 'react';
import {View, StyleSheet} from 'react-native'; 
import {
    Container, 
    Spinner,
    Text
} from 'native-base';


class Prosses extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
                <View style={style.content}>
                    <View >
                        <Spinner/>
                        <Text h2 style={style.text}>Mohon Tunggu</Text>
                    </View>
                </View>
        );
    }
}


export default Prosses; 

const style = StyleSheet.create({
    content : {
        width: 150, 
        maxHeight: 150, 
        backgroundColor:'#d9eeec',
        justifyContent:'center',
        alignContent:'center',
        borderRadius:10,
    },  
    loading : {
        justifyContent:'center',
        alignContent:'center',
    }, 
    text : {
        justifyContent:'center', 
        alignItems:'center', 
        textAlign:'center', 
        paddingBottom:10,
        color:'#000'
    }

});