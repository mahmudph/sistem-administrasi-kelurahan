import React,{Component} from 'react';
import {View, StyleSheet} from 'react-native'; 

import { allLogo } from '@assets'
import {

    Text
} from 'native-base';


class Prosses extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={style.content}>
                <View style={this.props.style}>
                    {this.props.children}
                    
                </View>
            </View>
        );
    }
}


export default Prosses; 

const style = StyleSheet.create({
    content : {
        width: 150, 
        maxHeight: 150, 
        backgroundColor:'#d9eeec',
        justifyContent:'center',
        alignContent:'center',
        borderRadius:10,
    },  
    loading : {
        justifyContent:'center',
        alignContent:'center',
    }, 
    
    

});