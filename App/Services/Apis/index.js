import xhr from './axios'
import {
  URI_WARGA_ANALYSIS,
  URI_WARGA_SEARCH,
  URI_WARGA_MENINGGAL,
  URI_WARGA_NEW_DEATH,        
  URI_SURAT_ANALYSIS,      
  URI_SURAT_NEW_PENGANTAR,
  URI_LOG_SURAT_REJECTED_LIST,
  URI_LOGS_SURAT_TERBUAT,
  URI_UPLOAD_IMAGE,    
  URL_CHANGE_NOMER,          
  URL_REGISTER,          
  URL_LOGIN,              
  URL_CHANGE_PASSWORD,
  URL_TOKEN_AUTH,       
  URL_PROFILE_USER,
  URL_FOR_AUTH_APP,
  URI_WARGA_DESCRIPTION,
  URL_FOR_DELETE_SURAT,
  URL_FOR_UPDATE_SURAT,
  URL_FOR_UPDATE_SAVE_SURAT,
  URL_PEMBERITAHUAN_INFO,
  URI_LOGS_SURAT_FINISHED
} from '../../Configs/Api'


/* ACTVATION AKUN */
export const postRegister = (data) => {
  return xhr(URL_REGISTER, 'POST', data)
}

/* LOGIN AUTENTICATION */
export const postLogin = (data) => {
  return xhr(URL_LOGIN, 'POST', data)
}

/* CHANGE PASSWORD RT */
export const ChangePasswordAuth = (data) => {
  return xhr(URL_CHANGE_PASSWORD, 'POST', data);
}

/* AUTENTICATION TOKEN */
export const AuthToken = (data) => {
  return xhr(URL_TOKEN_AUTH, 'POST', data);
}

/* PROFILE USERS RT */
export const GetProfile = async (data) => {
  return await xhr(URL_PROFILE_USER, 'GET', data);

}

/* GET WARGA BY KEYWORDS */
export const get_warga = (data) => {
  return xhr(URI_WARGA_SEARCH, 'POST', data);
}

/* upload image persyaratan */
export const uploadImages = (data) => {
  return xhr(URI_UPLOAD_IMAGE, 'POST', data);
}
/* new surat pengantar from rt */
export const new_pengantar_surat = (data) => {
  return xhr(URI_SURAT_NEW_PENGANTAR, 'POST', data);
}

/* ganti nomer kartu pak rt */
export const changeNomer = (data) => {
  return xhr(URL_CHANGE_NOMER, 'POST', data);
}


/* show logs surat terbuat belum terautentication */
export const ShowLogs = (data) => {
  return xhr(URI_LOGS_SURAT_TERBUAT, 'POST', data);
}

/* list logs surat tidak diterima  */
export const surat_rejected = (data) => {
  return xhr(URI_LOG_SURAT_REJECTED_LIST, 'POST', data);
}

/* informasi mengenai jumlah surat yang tebuat atau tereject  */
export const logs_surat_analysis = (data) => {
  return xhr(URI_LOGS_SURAT_TERBUAT, 'POST', data);
}


/* iformasi mengenai jumlah warga dan kk */
export const warga_info_jmlah= () => {
  return xhr(URI_WARGA_ANALYSIS, 'GET');
}

/* tampilkan infromasi mengenai warga secara detail */
export const warga_description = (id) => {
  return xhr(`${URI_WARGA_DESCRIPTION}${id}`, 'GET');
}

/* TAMPILKAN ANALYSIS SURAT YANG TELAH TERBUAT */
export const surat_analysis = () => {
  return xhr(URI_SURAT_ANALYSIS, 'GET', {});
}


/* TAMPILKAN WARGA YANG TELAH MENINGGAL  */
export const wargaMeninggalList = () => {
  return xhr(URI_WARGA_MENINGGAL, 'GET');
}



/* RUBAH STATUS DASAR WARGA BERGUNA SAAT MEMBUAT SURAT KEMATIAN */
export const newWargaMeninggal = () => {
  return xhr(URI_WARGA_NEW_DEATH, 'GET');
}

export const auth_app = () => {
  return xhr(URL_FOR_AUTH_APP, 'POST');
}

export const edit_surat =(id, id_jenis_surat) => {
  let url = `${URL_FOR_UPDATE_SURAT}?id=${id}&id_jenis_surat=${id_jenis_surat}`;
  return xhr(url, 'GET');
}


export const hapus_surat = (body) => {
  return xhr(URL_FOR_DELETE_SURAT, 'POST', body);
}

export const save_update = (body) => {
  return xhr(URL_FOR_UPDATE_SAVE_SURAT, 'POST', body);
}

export const get_pemberitahuan_info = () => {
  return xhr(URL_PEMBERITAHUAN_INFO, 'GET');
}


export const get_surat_finished = () => {
  return xhr(URI_LOGS_SURAT_FINISHED, 'GET');
}