import axios from 'axios'
import {checkToken} from '@AuthCheck';



const xhr = async (url, method, data, headers) => {
  try { 
    const token = await checkToken();
    let defaultHeader = {
      'Content-Type': 'application/json',
      'authorization': token !== null ? token : `Barrer ${token}`,
      'Accept': 'application/json',
    },
    config = {
      method,
      url,
      headers: headers ? headers : defaultHeader,
      data,
    }
    return await axios(config);

  } catch (err) {

    console.log(err);
  }
}

export default xhr
