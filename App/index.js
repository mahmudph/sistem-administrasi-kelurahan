import React from 'react';
import Navigation from './navigation';
import {Root} from 'native-base';
import {store_info} from '@redux_store';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';


const store = createStore(store_info, applyMiddleware(thunk));
export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Root>
            <Navigation />
        </Root>
      </Provider>
    );
  }
}
